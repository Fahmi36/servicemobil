<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="../assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title><?=$title?></title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="<?=base_url('assets/')?>css/select2.min.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="<?=base_url('assets/')?>css/bootstrap.min.css" rel="stylesheet" />	
    <link href="<?=base_url('assets/')?>css/now-ui-dashboard.min.css?v=1.5.0" rel="stylesheet" />	
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?=base_url('assets/')?>demo/demo.css" rel="stylesheet" />

    <script src="<?=base_url('assets/')?>js/core/jquery.min.js" ></script>
</head>

<body class="">
    <div class="page-loader" id="page-loader">
        <div>
            <div id="preloader">
                <div id="loadercss"></div>
            </div>
            <p id="text-loader"></p>
        </div>
    </div> 
    <div class="wrapper">
      <div class="sidebar" data-color="orange">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
    <div class="logo">
    	<img src="<?=base_url('assets/img/logo.png')?>">
    </div>
    <div class="sidebar-wrapper" id="sidebar-wrapper">
    	<ul class="nav">
    		<li class="<?= ($this->uri->segment(2)=='dashboard')?'active':null;  ?>" >
    			<a href="<?=site_url('admin/dashboard') ?>">
    				<i class="now-ui-icons design_app"></i>
    				<p>Dashboard</p>
    			</a>
    		</li>
    		<!-- Role SuperVisor -->

        <?php if ($this->session->userdata('role') == '1' OR $this->session->userdata('role') == '4'): ?>
    		<li class="<?= ($this->uri->segment(1)=='data_wo')?'active':null;  ?>">
    			<a href="<?=site_url('data_wo')?>">
    				<i class="now-ui-icons location_map-big"></i>
    				<p>Data WO</p>
    			</a>
    		</li>
      <?php endif ?>
    		<!-- End Role supervisor -->

    		<!-- Petugas Gudang -->

        <?php if ($this->session->userdata('role') == '1' OR $this->session->userdata('role') == '3'): ?>
    		<li class="<?= ($this->uri->segment(1)=='data_part')?'active':null;  ?>">
    			<a href="<?=site_url('data_part')?>">
    				<i class="now-ui-icons education_atom"></i>
    				<p>Kelola Spare Part</p>
    			</a>
    		</li>
      <?php endif ?>
      <?php if ($this->session->userdata('role') == '1' OR $this->session->userdata('role') == '4' OR $this->session->userdata('role') == '3'): ?>
        <li class="<?= ($this->uri->segment(1)=='data_pesan_part')?'active':null;  ?>">
          <a href="<?=site_url('data_pesan_part')?>">
            <i class="now-ui-icons education_atom"></i>
            <p>Data Pesanan Part </p>
          </a>
        </li> 

      <?php endif ?>
    		<!-- End Pengelola Gudang -->

    		<!-- Foreman -->
    		<!-- <li class="<?= ($this->uri->segment(1)=='data_kendaraan')?'active':null;  ?>">
    			<a href="<?=site_url('data_kendaraan')?>">
    				<i class="now-ui-icons education_atom"></i>
    				<p>Kelola Data Kendaraan</p>
    			</a>
    		</li> -->
        <?php if ($this->session->userdata('role') == '1' OR $this->session->userdata('role') == '2'): ?>
          
    		<li class="<?= ($this->uri->segment(1)=='data_kerusakan')?'active':null;  ?>">
    			<a href="<?=site_url('data_kerusakan')?>">
    				<i class="now-ui-icons education_atom"></i>
    				<p>Kelola Data Penjadwalan</p>
    			</a>
    		</li>
        <?php endif ?>
        <!-- <li class="<?= ($this->uri->segment(1)=='data_rata')?'active':null;  ?>">
          <a href="<?=site_url('data_rata')?>">
            <i class="now-ui-icons education_atom"></i>
            <p>Data Rata-Rata</p>
          </a>
        </li> 
        <li class="<?= ($this->uri->segment(1)=='pengaturan')?'active':null;  ?>">
    			<a href="<?=site_url('pengaturan')?>">
    				<i class="now-ui-icons education_atom"></i>
    				<p>Pengaturan Akun</p>
    			</a>
    		</li>-->
    		<li class="<?= ($this->uri->segment(1)=='logout')?'active':null;  ?>">
    			<a href="<?=site_url('logout')?>">
    				<i class="now-ui-icons education_atom"></i>
    				<p>Keluar</p>
    			</a>
    		</li>    		
    		<!-- End Foreman -->
    	</ul>
    </div>
</div>
<div class="main-panel" id="main-panel">
   <!-- Navbar -->
   <nav class="navbar navbar-expand-lg bg-primary  navbar-absolute">
      <div class="container-fluid">
         <div class="navbar-wrapper">
            <div class="navbar-toggle">
               <button type="button" class="navbar-toggler">
                  <span class="navbar-toggler-bar bar1"></span>
                  <span class="navbar-toggler-bar bar2"></span>
                  <span class="navbar-toggler-bar bar3"></span>
              </button>
          </div>
          <a class="navbar-brand" href="#pablo">Dashboard</a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navigation">
<!--         <form>
           <div class="input-group no-border">
              <input type="text" value="" class="form-control" placeholder="Search...">
              <div class="input-group-append">
                 <div class="input-group-text">
                    <i class="now-ui-icons ui-1_zoom-bold"></i>
                </div>
            </div>
        </div>
    </form> -->
    <ul class="navbar-nav">
<!--        <li class="nav-item">
          <a class="nav-link" href="#pablo">
             <i class="now-ui-icons media-2_sound-wave"></i>
             <p>
                <span class="d-lg-none d-md-block">Stats</span>
            </p>
        </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="now-ui-icons location_world"></i>
         <p>
            <span class="d-lg-none d-md-block">Some Actions</span>
        </p>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
     <a class="dropdown-item" href="#">Action</a>
     <a class="dropdown-item" href="#">Another action</a>
     <a class="dropdown-item" href="#">Something else here</a>
 </div>
</li> -->
<li class="nav-item">
  <a class="nav-link" href="#pablo">
     <i class="now-ui-icons users_single-02"></i>
     <p>
        <span class="d-lg-none d-md-block">Account</span>
    </p>
</a>
</li>
</ul>
</div>
</div>
</nav>
<!-- End Navbar -->
<!-- panel-header-lg -->
<div class="panel-header" data-color="orange">
  <!-- <canvas id="bigDashboardChart"></canvas> -->
</div>
<div class="content">


  <?php $this->load->view($link_view); ?>

</div>
<!-- <footer class="footer">
  <div class=" container-fluid ">
     <nav>
        <ul>
           <li>
              <a href="https://www.creative-tim.com">
                 Creative Tim
             </a>
         </li>
         <li>
          <a href="http://presentation.creative-tim.com">
             About Us
         </a>
     </li>
     <li>
      <a href="http://blog.creative-tim.com">
         Blog
     </a>
 </li>
</ul>
</nav>
<div class="copyright" id="copyright">
    &copy; <script>
       document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
   </script>, Designed by <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
</div>
</div>
</footer> -->
</div>
</div>
<script type="text/javascript">
    var BASE_URL = "<?=site_url('/')?>";
</script>
<!--   Core JS Files   -->
<script src="<?=base_url('assets/')?>js/core/popper.min.js" ></script>

<script src="<?=base_url('assets/')?>js/core/bootstrap.min.js" ></script>

<script src="<?=base_url('assets/')?>js/plugins/perfect-scrollbar.jquery.min.js" ></script>
<script src="<?=base_url('assets/')?>js/plugins/moment.min.js"></script>		<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?=base_url('assets/')?>js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for Sweet Alert -->
<script src="<?=base_url('assets/')?>js/plugins/sweetalert2.min.js"></script>

<script src="<?=base_url('assets/')?>js/select2.full.min.js"></script>
<!-- Forms Validations Plugin -->
<script src="<?=base_url('assets/')?>js/plugins/jquery.validate.min.js"></script>
<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
<script src="<?=base_url('assets/')?>js/plugins/jquery.bootstrap-wizard.js"></script>
<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script src="<?=base_url('assets/')?>js/plugins/bootstrap-selectpicker.js" ></script>
<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
<script src="<?=base_url('assets/')?>js/plugins/bootstrap-datetimepicker.js"></script>
<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
<script src="<?=base_url('assets/')?>js/plugins/jquery.dataTables.min.js"></script>
<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="<?=base_url('assets/')?>js/plugins/bootstrap-tagsinput.js"></script>
<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?=base_url('assets/')?>js/plugins/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
<script src="<?=base_url('assets/')?>js/plugins/fullcalendar.min.js"></script>
<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
<script src="<?=base_url('assets/')?>js/plugins/jquery-jvectormap.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?=base_url('assets/')?>js/plugins/nouislider.min.js" ></script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>

<!--  Notifications Plugin    -->
<script src="<?=base_url('assets/')?>js/plugins/bootstrap-notify.js"></script>		<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="<?=base_url('assets/')?>js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="<?=base_url('assets/')?>demo/demo.js"></script>

<!-- Sharrre libray -->
<script src="<?=base_url('assets/')?>demo/jquery.sharrre.js"></script>
</body>

</html>