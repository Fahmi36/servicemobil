        
<link type="text/css" rel="stylesheet" href="<?=base_url('assets/css/scheduler_8.css')?>"/>
<div class="row">
  <div class="card  card-tasks">
    <div class="card-body">

      <div id="dp"></div>
       <div class="card-footer">
        <span class="text-red">** Seluruh Karyawan Istirahat Pada Pukul 12.00 - 13.00 WIB</span>
      </div>
    </div>
  </div>
    <div class="card card-tasks">
    <div class="col-md-12">
      <!-- <div class="card-header">
        <h4>Perhitungan Rerata</h4>
      </div> -->
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped dataTable">
            <thead>
              <th>Rata-rata Waktu Tunggu (Hari)</th>
              <th>Rata-rata Waktu Selesai (Hari)</th>
              <th>Utilitas (%)</th>
              <th>Job Rata-rata </th>
            </thead>
            <tbody id="contentNya">
              <?php foreach ($status->result() as $key): ?>
                <tr>
                  <td><?=$key->rerata_t?></td>
                  <td><?=$key->rerata_s?></td>
                  <td><?=($key->utilitas / 8)?> %</td>
                  <td><?=($key->job)?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="modal fade bd-example-modal-lg" id="LihatModal" tabindex="-1" role="dialog" aria-labelledby="LihatModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LihatModalTitle">Lihat Data WO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editdatawo" action="javascript:void(0);" method="post">
          <hr>
          <center><h3>Info Pelanggan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Nama Pemilik</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmpemiliktext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Nama di STNK</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="namastnktext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Alamat</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="alamattext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Telepon</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="telptext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No NPWP</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nonpwptext"></label>
              </div>
            </div>
          </div>
          <hr>
          <center><h3>Info Kendaraan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Merk Kendaraan</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="merkkentext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Polisi</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nopolisitext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Rangka</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="norangkatext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Mesin</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nomesintext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Model</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="modelkentext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Type</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="typekenttext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Warna</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="warnatext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Bahan Bakar</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="typetext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Tanggal DEC</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="tglbelitext"></label>
              </div>
            </div>
          </div>
          <hr>
          <center><h3>Order Pekerjaan</h3></center>
          <div class="form-group" id="nmpekerjaantxt">

          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Spare Part</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmparttext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Pembuat data Work Order</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="namauserttext"></label>
              </div>
            </div>
          </div>
          <hr>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <?php if ($this->session->userdata('role') == '1' OR $this->session->userdata('role') == '2'): ?>
          <a id="accnya" href="javascript:void(0);" class="btn btn-info">Selesai</a>
        <?php endif ?>
      </form>
    </div>
  </div>
</div>
</div>

<script src="<?=base_url('assets/js/daypilot-all.min.js')?>"></script>
<script type="text/javascript">
  var URL = '<?=site_url('/')?>';
  var DateNo = "<?=date('Y-m-01')?>";
  var dp = new DayPilot.Scheduler("dp");
  
  dp.theme = "scheduler_8";
  dp.cellWidth = 40;
  dp.startDate =  new DayPilot.Date(DateNo);
  dp.days = dp.startDate.daysInMonth();
  dp.scale = "Hour";
  dp.timeHeaders = [
  {groupBy: "Day"},
  {groupBy: "Hour", format: "H:00"}
  ];
   dp.onBeforeEventRender = function(args) {
       if(args.e.status == 2){
           args.data.backColor = "#09b2ef";
       }else{
           args.data.backColor = "#008000";
       }
  };
  dp.onIncludeTimeCell = function(args) {
    if (args.cell.start.getHours() === 12 || args.cell.start.getHours() <= 7 || args.cell.start.getHours() >= 18) {

      args.cell.visible = false;
    }
  };
  dp.bubble = new DayPilot.Bubble({
    onLoad: function (args) {
      var ev = args.source;
      args.async = true; 

    }
  });
  dp.events.list = [];

  dp.treeEnabled = true;

  dp.resources =
  [
  {name: "Stall 1", id: "Stall 1",eventHeight:70},
  {name: "Stall 2", id: "Stall 2",eventHeight:70},
  {name: "Stall 3", id: "Stall 3",eventHeight:70},
  ];
  dp.onEventClick = function (args) {
    LihatData(args.e.id());
  };
  
  dp.init();

  loadEvents();

  function loadEvents() {
    dp.events.load(URL + 'AdminController/qDataWO');
  }

  function DataSelesai(id) {
    $.ajax({
      url: BASE_URL + 'AdminController/SelesaiInstruksi',
      type: 'POST',
      dataType: 'json',
      data: {id: id},
      success:function (args) {
        if (args.success) {
          Swal.fire(
            'Berhasil!',
            ''+args.message+'',
            'success'
            )
            location.reload();
        }else{
          Swal.fire(
            'Gagal!',
            ''+args.message+'',
            'danger'
            )
        }
      },error:function(error) {
        Swal.fire(
          'Error!',
          'Silakan Klik Ulang',
          'info'
          )
      }
    })

  }
  function LihatData(id) {
    $.ajax({
      url: BASE_URL + 'AdminController/getDataWO',
      type: 'post',
      dataType: 'json',
      data: {id: id},
      success:function (data) {
        if (data.success) {
          $('#alamattext').text(data.data[0].alamat);
          $('#nmparttext').text(data.data[0].part);
          $('#estimasitext').text(data.data[0].estimasi_biaya);
          $('#merkkentext').text(data.data[0].merk_kendaraan);
          $('#modelkentext').text(data.data[0].model_kendaraan);
          $('#namastnktext').text(data.data[0].nama_stnk);
          $('#nmpemiliktext').text(data.data[0].nm_pemilik);
          $('#nomesintext').text(data.data[0].no_mesin);
          $('#nonpwptext').text(data.data[0].no_npwp);
          $('#nopolisitext').text(data.data[0].no_polisi);
          $('#norangkatext').text(data.data[0].no_rangka);
          $('#nowotext').text(data.data[0].no_wo);
          $('#stalltext').text(data.data[0].stall);
          $('#statuskerjatext').text(data.data[0].status_kerja);
          $('#telptext').text(data.data[0].telepon);
          $('#tglbelitext').text(data.data[0].tgl_pembelian);
          $('#tglselesaitext').text(data.data[0].tgl_selesai);
          $('#tglterimatext').text(data.data[0].tgl_terima);
          $('#typetext').text(data.data[0].type_bbm);
          $('#typekenttext').text(data.data[0].type_kendaraan);
          $('#wmulaitext').text(data.data[0].w_mulai);
          $('#wprosestext').text(data.data[0].w_proses);
          $('#wtunggutext').text(data.data[0].w_tunggu);
          $('#warnatext').text(data.data[0].warna);
          $('#namauserttext').text(data.data[0].namauser);
          if (data.data[0].status_pekerjaan == '2') {
            $('#accnya').attr('onclick', 'DataSelesai(\''+id+'\')');;
          }else{
            $('#accnya').removeAttr('onclick');
          }
          $("#LihatModal").modal({backdrop:'static',keyboard:false});
          $.ajax({
            url: BASE_URL + 'AdminController/getInstruksi',
            type: 'POST',
            dataType: 'json',
            data: {id: id},
            success:function(jenis) {
                  // debugger
                  for (var i = 0; i < jenis.data.length; i++) {
                    $('#nmpekerjaantxt').html('<div class="row"><div class="col-md-3"><label>Instruksi Pekerjaan '+(i +1)+'</label></div><div class="col-md-1" style="max-width: 3.33333%;"><label>:</label></div><div class="col-md-7"><label>'+jenis.data[i].nm_pekerjaan+'</label></div><div class="col-md-3"><label>Kategori Pekerjaan '+(i +1)+'</label></div><div class="col-md-1" style="max-width: 3.33333%;"><label>:</label></div><div class="col-md-7"><label>'+jenis.data[i].id_kategori+'</label></div></div>') 
                  }
                }
              })
        }else{
          Swal.fire(
            'Error!',
            'Silakan Klik Ulang',
            'info'
            )
        }
      },error:function(error) {
        Swal.fire(
          'Error!',
          'Silakan Klik Ulang',
          'info'
          )
      }
    })
  }
</script>