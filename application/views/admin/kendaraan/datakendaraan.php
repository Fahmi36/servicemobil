<div class="row">
  <div class="card card-tasks">
    <div class="col-md-12">
      <div class="card-header">
        <h4>Daftar Kendaraan </h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped dataTable">
            <thead>
              <th>Nama Stnk</th>
              <th>No Polisi</th>
              <th>No Rangka</th>
              <th>Warna</th>
              <th>Model Kendaraan</th>
              <th>Merk Kendaraan</th>
              <th>Type Kendaraan</th>
              <th>No Mesin</th>
              <th>Tgl Pembelian</th>
              <th>Type BBM</th>
              <th>Tindakan</th>
            </thead>
            <tbody id="contentNya">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="EditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EditModalTitle">Edit Data WO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editdatawo" action="javascript:void(0);" method="post">
          <div class="form-group">
            <label for="inputAddress">Nama di STNK</label>
            <input type="text" class="form-control" id="inputAddress" placeholder="Tanggal Penerimaan">
          </div>
          <div class="form-group">
            <label for="inputAddress">No Polisi</label>
            <input type="text" class="form-control" id="inputAddress" placeholder="Tanggal Penerimaan">
          </div>
          <div class="form-group">
            <label for="inputAddress2">No Rangka</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Warna</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Model Kendaraan</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Merk Kendaraan</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Type Kendaraan</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">No Mesin</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Tanggal Pembelian</label>
            <input type="date" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Type BBM</label>
            <input type="text" name="tgl_spp" class="form-control" id="inputAddress2" placeholder="Tanggal SPP">
          </div>
          <hr>
          <button type="submit" class="btn btn-primary">Edit</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  jQuery(document).ready(function($) {
    wp.dataALLKen();
  });
  function LihatKen(id) {
    $.ajax({
      url: BASE_URL + 'AdminController/getDataWO',
      type: 'post',
      dataType: 'json',
      data: {id: id},
      success:function (data) {
        if (data.success) {
          $("#viewModal").modal({backdrop:'static',keyboard:false});
        }else{
          Swal.fire(
          'Error!',
          'Silakan Klik Ulang',
          'info'
          )
        }
      },error:function(error) {
        Swal.fire(
          'Error!',
          'Silakan Klik Ulang',
          'info'
          )
      }
    })
  }
  function EditKen(id) {
     $.ajax({
      url: BASE_URL + 'AdminController/getDataWO',
      type: 'post',
      dataType: 'json',
      data: {id: id},
      success:function (data) {
        if (data.success) {
          $("#EditModal").modal({backdrop:'static',keyboard:false});
        }else{

        }
      }
    })
  }
</script>