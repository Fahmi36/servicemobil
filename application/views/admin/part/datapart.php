<style type="text/css">
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 32px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 33px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    background-color:transparent;     
    color:#fff;
    padding: 0 0 0 10px;
  }
  .select2-container .select2-selection--single {
    height: 35px;
  }
  .select2-container--default .select2-selection--single {
    border: none;
    border-radius: 0;
    border-bottom: solid 1px #fff;
    background: transparent;
  }
  .select2-container {
    display:block;
    width:100% !important; 
  }
  .select2-container--default .select2-selection--single .select2-selection__placeholder {
    color: #fff;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #fff transparent transparent transparent !important;
  }
</style>
<div class="row">
  <div class="card card-tasks">
    <div class="col-md-12">
      <div class="card-header">
        <h4>Daftar Part</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped dataTable"><a class="text-white btn btn-primary" onclick="modalForm()">Tambah</a>
            <thead>
              <th>Nama Part</th>
              <th>Jumlah Part</th>
              <th>Tindakan</th>
            </thead>
            <tbody id="contentNya">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="modalpart" tabindex="-1" role="dialog" aria-labelledby="modalpartTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalpartTitle">Input Data Part</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="datakelolapart" action="javascript:void(0);" method="post">
          <hr>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Nama Part</label>
              <input type="text" autocomplete="off" class="form-control" name="namapart" placeholder="Nama Part">
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress">Jumlah Part</label>
            <input type="number" autocomplete="off"  class="form-control" name="jmlpart" placeholder="Jumlah Part">
          </div>
<!--           <div class="form-group">
            <label for="inputAddress2">Status Part</label>
            <select name="status" id="status" class="form-control"> 
              <option value="aktif">Aktif</option>
              <option value="tidak">Tidak Aktif</option>
            </select>
          </div> -->
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="EditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EditModalTitle">Edit Data Part</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <form id="editdataPart" action="javascript:void(0);" method="post">
          <div class="form-group">
            <label for="inputAddress">Part</label>
            <input type="text" class="form-control" name="part" autocomplete="off" id="partnya" placeholder="Masukan Part">
            <input type="hidden" class="form-control" name="idpart" autocomplete="off" id="idpartnya" placeholder="Masukan Part">
          </div>
          <div class="form-group">
            <label for="inputAddress">Jumlah Part</label>
            <input type="text" class="form-control" name="jmlart" autocomplete="off" id="jmlpartnya" placeholder="Masukan Part">
          </div>

          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="LihatModal" tabindex="-1" role="dialog" aria-labelledby="LihatModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LihatModalTitle">Lihat Data Part</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center><h3>Info Part</h3></center>
        <div class="form-group">
          <div class="row">
            <div class="col-md-3">
              <label>Nama Part</label>
            </div>
            <div class="col-md-1" style="max-width: 3.33333%;">
              <label>:</label>
            </div>
            <div class="col-md-7">
              <label id="nmpart"></label>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-md-3">
              <label>Jumlah Part</label>
            </div>
            <div class="col-md-1" style="max-width: 3.33333%;">
              <label>:</label>
            </div>
            <div class="col-md-7">
              <label id="jmlparttext"></label>
            </div>
          </div>
        </div>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>

    </div>
  </div>
</div>
<script>
  jQuery(document).ready(function($) {
    wp.dataPart();
  });
  function modalForm() {
    $("#modalpart").modal({backdrop:'static',keyboard:false});
  }
  function LihatPart(id) {
   $.ajax({
    url: BASE_URL +'AdminController/DataPartALL',
    type: 'POST',
    dataType: 'JSON',
    data: {id: id},
    success:function (data) {
      $('#nmpart').text(data.data[0].nm_part);
      $('#jmlparttext').text(data.data[0].jml_part);
      $("#LihatModal").modal({backdrop:'static',keyboard:false});
    }
  })
 }
 function EditPart(id) {
  $.ajax({
    url: BASE_URL +'AdminController/DataPartALL',
    type: 'POST',
    dataType: 'JSON',
    data: {id: id},
    success:function (data) {
      $('#idpartnya').val(data.data[0].href);
      $('#partnya').val(data.data[0].nm_part);
      $('#jmlpartnya').val(data.data[0].jml_part);
      $("#EditModal").modal({backdrop:'static',keyboard:false});
    }
  })

      //
    }
  </script>