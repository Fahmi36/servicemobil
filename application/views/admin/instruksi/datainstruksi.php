
<div class="row">
  <div class="col-md-12">
    <div class="card  card-tasks">
      <div class="card-header ">
        <h5 class="card-category">Work Order</h5>
        <h4 class="card-title">Data</h4>
      </div>
      <div class="card-body ">
        <form>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Gejala Pertama Kali Terdeteksi</label>
              <textarea name="gejala_awal" class="form-control" colspan="5" autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress">Frekuensi Kejadian</label>
            <input type="number" class="form-control" id="inputAddress" placeholder="1234 Main St">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Mil Saat Gejala Terjadi</label>
            <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
          </div>
          <hr>
          <label><b>Kondisi Kendaraan Saat Gejala Terjadi</b></label>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputCity">Kecepatan</label>
              <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-12">
              <label for="inputState">Kondisi Mesin</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-12">
              <label for="inputZip">Lainnya</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
             <div class="form-group col-md-12">
              <label for="inputZip">Posisi shift Lever</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
          </div>
          <hr>
          <label><b>WAC Walk Around Check)</b></label>
          <div class="row">
              <div class="col-md-6">X : Rusak</div>
              <div class="col-md-6">@ : Penyok</div>
              <div class="col-md-6"># : Baret</div>
              <div class="col-md-6">? : Tidak Ada</div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputCity">Ban Serep</label>
              <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-6">
              <label for="inputState">KM</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputZip">CD / Kaset</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
             <div class="form-group col-md-6">
              <label for="inputZip">Uang</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputCity">Payung</label>
              <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-6">
              <label for="inputState">STNK</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputZip">Dongkrak</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
             <div class="form-group col-md-6">
              <label for="inputZip">Buku Service</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputCity">P3K</label>
              <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-6">
              <label for="inputState">Alarm</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputZip">Kunci Steer</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
             <div class="form-group col-md-6">
              <label for="inputZip">AC</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputCity">Tool Set</label>
              <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-6">
              <label for="inputState">Power Window</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
            <div class="form-group col-md-6">
              <label for="inputZip">Clip Karpet</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
          </div>
          <div class="form-group col-md-12">
              <label for="inputZip">Lainnya</label>
              <textarea class="form-control" name="lainnya"> </textarea>
            </div>
            <label><b>Order Pekerjaan</b></label>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputCity">Deskripsi Pekerjaan</label>
              <input type="text" class="form-control" id="inputCity">
            </div>
            <div class="form-group col-md-6">
              <label for="inputState">Biaya</label>
              <input type="text" class="form-control" id="inputZip">
            </div>
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" value="">
                Check me out
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
              </label>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Sign in</button>
        </form>
      </div>
      <div class="card-footer ">
        <hr>
        <div class="stats">
          <i class="now-ui-icons loader_refresh spin"></i> Updated 3 minutes ago
        </div>
      </div>
    </div>
  </div>
</div>