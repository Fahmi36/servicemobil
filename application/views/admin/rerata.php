<div class="row">
  <div class="card card-tasks">
    <div class="col-md-12">
      <div class="card-header">
        <h4>Perhitungan Rerata</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped dataTable">
            <thead>
              <th>Rata-rata Waktu Tunggu (Hari)</th>
              <th>Rata-rata Waktu Selesai (Hari)</th>
              <th>Utilitas (%)</th>
              <th>Job Rata-rata </th>
            </thead>
            <tbody id="contentNya">
            	<?php foreach ($status->result() as $key): ?>
            		<tr>
            			<td><?=round($key->rerata_t,2)?></td>
            			<td><?=round($key->rerata_s,2)?></td>
            			<td><?=round($key->utilitas,2)?> %</td>
            			<td><?=round($key->job,2)?></td>
            		</tr>
            	<?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>