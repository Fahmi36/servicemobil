<style type="text/css">
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 32px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 33px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    background-color:transparent;     
    padding: 0 0 0 10px;
  }
  .select2-container .select2-selection--single {
    height: 35px;
  }
  .select2-container {
    display:block;
    width:100% !important; 
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #fff transparent transparent transparent !important;
  }
  #autoSuggestionsList > a li {
    background: none repeat scroll 0 0 #fff;
    border-bottom: 1px solid #E3E3E3;
    list-style: none outside none;
    padding: 8px 8px 8px 14px;
  }
  #autoSuggestionsList > a li:hover{ 
    background: #fe6937 !important; color: #fff; 
  }
  #autoSuggestionsList > a { 
    color: #333;text-decoration: none; 
  }
  #autoSuggestionsListpart > a li {
    background: none repeat scroll 0 0 #fff;
    border-bottom: 1px solid #E3E3E3;
    list-style: none outside none;
    padding: 8px 8px 8px 14px;
  }
  #autoSuggestionsListpart > a li:hover{ 
    background: #fe6937 !important; color: #fff; 
  }
  #autoSuggestionsListpart > a { 
    color: #333;text-decoration: none; 
  }
</style>
<div class="row">
  <div class="card card-tasks">
    <div class="col-md-12">
      <div class="card-header">
        <h4>Daftar Penjadwalan </h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped dataTable">
            <thead>
              <th>No WO</th>
              <th>Stall</th>
              <th>Nomor Polisi</th>
              <th>Waktu Mulai</th>
              <th>Waktu Tunggu</th>
              <th>Tanggal Terima</th>
              <th>Tanggal Selesai</th>
              <!-- <th>Booking</th> -->
              <th>Tindakan</th>
            </thead>
            <tbody id="contentNya">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="EditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EditModalTitle">Edit Data Penjadwalan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
        <form id="editdatajadwal" action="javascript:void(0);" method="post">
          <!-- <div class="form-group">
            <label for="inputAddress">Stall</label>
            <input type="text" class="form-control" name="stall" autocomplete="off" onkeyup="cariStall();" id="stallnya" placeholder="Masukan Stall">
            <div id="suggestions">
              <div id="autoSuggestionsList"></div>
            </div>
          </div> -->
          <div class="form-group">
            <label for="inputAddress">Waktu Di Terima</label>
            <input type="text" class="form-control" autocomplete="off" id="waktuterima" disabled placeholder="Tanggal Penerimaan">
            <input type="hidden" class="form-control" autocomplete="off" id="href" name="href">
          </div>
          <div class="form-group">
            <label for="inputAddress">Waktu Mulai</label>
            <input type="text" class="form-control" name="waktumulai" autocomplete="off" id="waktumulai" placeholder="Tanggal Mulai">
          </div>
          <div class="controls-part">
            <div class="forms-part">
              <div class="entry-part">

              </div>
            </div>
          </div>
          <div class="controls-clone-part">
            <div class="forms-clone-part">
              <div class="entry-clone-part">
                <div class="form-group">
                  <label for="inputAddress">Masukan Nama Part</label>
                  <select class="form-control" name="partbaru[]">
                    <option selected disabled="">Pilih Part</option>
                    <?php foreach ($part->result() as $key): ?>
                      <option value="<?=$key->id_part?>"><?=$key->nm_part?></option>
                    <?php endforeach ?>
                  </select>
                </div>
                <div class="col-xs-12 col-md-2" style="margin-bottom:10px;">
                  <button class="btn btn-info add-more-part" style="width:100%;">Tambah</button>
                </div>
              </div>
            </div>
          </div>
          <div class="controls">
            <div class="forms">
              <div class="entry">

              </div>
            </div>
          </div>
          <div class="controls-clone">
            <div class="forms-clone">
              <div class="entry-clone">
                <div class="form-group">
                  <label for="inputAddress">Pekerjaan</label>
                  <textarea class="form-control" autocomplete="off" name="instruksibaru[]" id="pekerjaan"></textarea>
                </div>
                <div class="form-group">
                  <label for="inputAddress">Jenis Pekerjaan</label>
                  <select class="form-control" autocomplete="off" id="jeniskerja" name="jeniskerjabaru[]">
                    <option disabled="" selected="">Pilih Jenis Pekerjaan</option>
                    <option value="Ringan">Ringan</option>
                    <option value="Sedang">Sedang</option>
                    <option value="Berat">Berat</option>
                  </select>
                </div>
               <!--  <div class="form-group">
                  <label for="inputAddress2">Waktu Proses</label>
                  <input type="number" name="prosesbaru[]" class="form-control" autocomplete="off" id="inputAddress2" placeholder="Masukan Waktu Proses Dalam Jam">
                </div> -->
                <div class="col-xs-12 col-md-2" style="margin-bottom:10px;">
                  <button class="btn btn-info add-more" style="width:100%;">Tambah</button>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <button type="submit" class="btn btn-primary">Edit</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="LihatModal" tabindex="-1" role="dialog" aria-labelledby="LihatModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LihatModalTitle">Lihat Data WO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editdatawo" action="javascript:void(0);" method="post">
          <hr>
          <center><h3>Info Pelanggan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Nama Pemilik</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmpemiliktext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Nama di STNK</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="namastnktext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Alamat</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="alamattext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Telepon</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="telptext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No NPWP</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nonpwptext"></label>
              </div>
            </div>
          </div>
          <hr>
          <center><h3>Info Kendaraan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Merk Kendaraan</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="merkkentext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Polisi</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nopolisitext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Rangka</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="norangkatext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Mesin</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nomesintext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Model</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="modelkentext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Type</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="typekenttext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Warna</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="warnatext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Bahan Bakar</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="typetext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Tanggal DEC</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="tglbelitext"></label>
              </div>
            </div>
          </div>
          <hr>
          <center><h3>Order Pekerjaan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Keluhan Pelanggan</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmkeluhantext"></label>
              </div>
            </div>
          </div>
          <div class="form-group" id="nmpekerjaantxt">

          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Spare Part</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmparttext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Pembuat data Work Order</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="namauserttext"></label>
              </div>
            </div>
          </div>
          <hr>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  jQuery(document).ready(function($) {
    $('#waktumulai').datetimepicker({
      format : 'DD-MM-YYYY HH:mm:ss',
      icons: {
        time: "now-ui-icons tech_watch-time",
        date: "now-ui-icons ui-1_calendar-60",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'now-ui-icons arrows-1_minimal-left',
        next: 'now-ui-icons arrows-1_minimal-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
      }
    });
    wp.dataALLJadwalService();
    $(document).on('click', '.add-more', function(e)
    {
     e.preventDefault();

     var controlForm = $('.controls-clone .forms-clone:first'),
     currentEntry = $(this).parents('.entry-clone:first'),
     newEntry = $(currentEntry.clone()).appendTo(controlForm);

     newEntry.find('input').val('');
     newEntry.find('textarea').val('');
     controlForm.find('.entry-clone:not(:last) .add-more')
     .removeClass('add-more').addClass('btn-remove')
     .removeClass('btn-info').addClass('red')
     .html('Hapus');
   }).on('click', '.btn-remove', function(e)
   {
     $(this).parents('.entry-clone:first').remove();

     e.preventDefault();
     return false;
   });
   $(document).on('click', '.add-more-part', function(e)
   {
     e.preventDefault();
     
     var controlForm = $('.controls-clone-part .forms-clone-part:first'),
     currentEntry = $(this).parents('.entry-clone-part:first'),
     newEntry = $(currentEntry.clone()).appendTo(controlForm);

     newEntry.find('input').val('');
     controlForm.find('.entry-clone-part:not(:last) .add-more-part')
     .removeClass('add-more-part').addClass('btn-remove-part')
     .removeClass('btn-info').addClass('red')
     .html('Hapus');
   }).on('click', '.btn-remove-part', function(e)
   {
     $(this).parents('.entry-clone-part:first').remove();

     e.preventDefault();
     return false;
   });
 });

  function LihatKen(id) {
   $.ajax({
    url: BASE_URL + 'AdminController/getDataWO',
    type: 'post',
    dataType: 'json',
    data: {id: id},
    success:function (data) {
      if (data.success) {
        $('#alamattext').text(data.data[0].alamat);
        $('#nmparttext').text(data.data[0].part);
        $('#estimasitext').text(data.data[0].estimasi_biaya);
        $('#merkkentext').text(data.data[0].merk_kendaraan);
        $('#modelkentext').text(data.data[0].model_kendaraan);
        $('#namastnktext').text(data.data[0].nama_stnk);
        $('#nmpemiliktext').text(data.data[0].nm_pemilik);
        $('#nomesintext').text(data.data[0].no_mesin);
        $('#nonpwptext').text(data.data[0].no_npwp);
        $('#nopolisitext').text(data.data[0].no_polisi);
        $('#norangkatext').text(data.data[0].no_rangka);
        $('#nowotext').text(data.data[0].no_wo);
        $('#stalltext').text(data.data[0].stall);
        $('#statuskerjatext').text(data.data[0].status_kerja);
        $('#telptext').text(data.data[0].telepon);
        $('#nmkeluhantext').text(data.data[0].keluhan);
        $('#tglbelitext').text(data.data[0].tgl_pembelian);
        $('#tglselesaitext').text(data.data[0].tgl_selesai);
        $('#tglterimatext').text(data.data[0].tgl_terima);
        $('#typetext').text(data.data[0].type_bbm);
        $('#typekenttext').text(data.data[0].type_kendaraan);
        $('#wmulaitext').text(data.data[0].w_mulai);
        $('#wprosestext').text(data.data[0].w_proses);
        $('#wtunggutext').text(data.data[0].w_tunggu);
        $('#warnatext').text(data.data[0].warna);
        $('#namauserttext').text(data.data[0].namauser);
        $("#LihatModal").modal({backdrop:'static',keyboard:false});
        $.ajax({
          url: BASE_URL + 'AdminController/getInstruksi',
          type: 'POST',
          dataType: 'json',
          data: {id: id},
          success:function(jenis) {
            for (var i = 0; i < jenis.data.length; i++) {
              $('#nmpekerjaantxt').html('<div class="row"><div class="col-md-3"><label>Instruksi Pekerjaan '+(i +1)+'</label></div><div class="col-md-1" style="max-width: 3.33333%;"><label>:</label></div><div class="col-md-7"><label>'+jenis.data[i].nm_pekerjaan+'</label></div><div class="col-md-3"><label>Kategori Pekerjaan '+(i +1)+'</label></div><div class="col-md-1" style="max-width: 3.33333%;"><label>:</label></div><div class="col-md-7"><label>'+jenis.data[i].id_kategori+'</label></div></div>'); 
            }
          }
        })
      }else{
        Swal.fire(
          'Error!',
          'Muat Ulang',
          'info'
          )
      }
    },error:function(error) {
      Swal.fire(
        'Error!',
        'Muat Ulang',
        'info'
        )
    }
  })
 }
 function EditKen(id) {
   $.ajax({
    url: BASE_URL + 'AdminController/getDataWO',
    type: 'post',
    dataType: 'json',
    data: {id: id},
    success:function (data) {
      if (data.success) {
        $("#stallnya").val(data.data[0].stall)
        $('#waktuterima').val(data.data[0].tgl_terima);
        $('#href').val(data.data[0].href);
        $('#pekerjaan').text(data.data[0].nm_pekerjaan);
        $.ajax({
          url: BASE_URL + 'AdminController/getTanggalMulai',
          type: 'POST',
          dataType: 'json',
          data: {id: id},
          success:function(jenis) {
            if (jenis.data != null) {
              $('#waktumulai').val(jenis.data);
           }else{
              $('#waktumulai').val();
          }
        // $.ajax({
        //   url: BASE_URL + 'AdminController/getInstruksi',
        //   type: 'POST',
        //   dataType: 'json',
        //   data: {id: id},
        //   success:function(jenis) {
        //     if (jenis.data.length > 0) {
        //       var loop = '';
        //       for (var i = 0; i < jenis.data.length; i++) {
        //        loop += '<div id="formhapus'+jenis.data[i].idinstruksi+'"><div class="form-group"><label for="inputAddress">Pekerjaan</label><textarea class="form-control" autocomplete="off" name="instruksi[]" id="pekerjaan">'+jenis.data[i].nm_pekerjaan+'</textarea></div><div class="form-group"><label for="inputAddress">Jenis Pekerjaan</label><select class="form-control" autocomplete="off" id="jeniskerja" name="jeniskerja[]"><option disabled="">Pilih Jenis Pekerjaan</option><option selected value="'+jenis.data[i].id_kategori+'">'+jenis.data[i].id_kategori+'</option><option value="Ringan">Ringan</option><option value="Sedang">Sedang</option><option value="Berat">Berat</option></select></div><input type="hidden" name="id_kategori[]" class="form-control" autocomplete="off" id="inputAddress2" value="'+jenis.data[i].idinstruksi+'"><div class="col-xs-12 col-md-2" style="margin-bottom:10px;"><a class="btn btn-secondary btn-remove" style="width:100%;" onclick="hapusform(\''+jenis.data[i].idinstruksi+'\');">Hapus</a></div></div></div>'; 
        //      }
        //      $('.entry').html(loop);
        //    }else{
        //     $('.entry-clone').html('<div class="form-group"><label for="inputAddress">Pekerjaan</label><textarea class="form-control" autocomplete="off" name="instruksibaru[]" id="pekerjaan"></textarea></div><div class="form-group"><label for="inputAddress">Jenis Pekerjaan</label><select class="form-control" autocomplete="off" id="jeniskerjabaru" name="jeniskerjabaru[]"><option disabled="" selected="">Pilih Jenis Pekerjaan</option><option value="Ringan">Ringan</option><option value="Sedang">Sedang</option><option value="Berat">Berat</option></select></div><div class="col-xs-12 col-md-2" style="margin-bottom:10px;"><button class="btn btn-info add-more" style="width:100%;">Tambah</button></div>');
        //   }
          $("#EditModal").modal({backdrop:'static',keyboard:false});
        },error:function(error) {
          Swal.fire(
            'Error!',
            'Muat Ulang',
            'info'
            )
        }
      })
      }
    },error:function(error) {
      Swal.fire(
        'Error!',
        'Muat Ulang',
        'info'
        )
    }
  })
 }
 function cariStall(e) { 
  $.ajax({
    url: BASE_URL + 'AdminController/getStall',
    type: 'POST',
    dataType: 'json',
    data: {q: $("#stallnya").val()},
    success:function(data) {
      var stall = [];
      var loop = "";
      for (var i in data) {
        stall.push(data[i].stall);
      }
      if (data[i].length > 0) {
        for (var a =0; a < data[i].length; a++) {
          if (data[i][a].stall != ''  && data[i][a].stall != null && data[i][a].stall != undefined) {
            $('#suggestions').show();
            $('#autoSuggestionsList').addClass('auto_list');
            loop += '<a href="javascript:void(0);" onclick="pilihStall(\''+data[i][a].stall+'\')"><li>'+data[i][a].stall+'</li></a>';
          }
        }
        $('#autoSuggestionsList').html(loop);      

      }
    }
  })
}
function pilihStall(id) {
  $("#stallnya").val(id); 
  $('#suggestions').hide();  
}
function cariPart(e) { 
  $.ajax({
    url: BASE_URL + 'AdminController/getPart',
    type: 'POST',
    dataType: 'json',
    data: {q: $("#partnya").val()},
    success:function(data) {
      var part = [];
      var loop = "";
      for (var i in data.data) {
        part.push(data.data[i].nm_part);
      }
      if (part.length > 0) {
        for (var a =0; a < part.length; a++) {
          $('#suggestionspart').show();
          $('#autoSuggestionsListpart').addClass('auto_list');
          loop += '<a href="javascript:void(0);" onclick="pilihPart(\''+part[a]+'\')"><li>'+part[a]+'</li></a>';
        }
        $('#autoSuggestionsListpart').html(loop);           
      }
    }
  })
}
function pilihPart(id) {
  $("#partnya").val(id); 
  $('#suggestionspart').hide();  
}
function hapusform(id) {
  Swal.fire({
    title: 'Apakah Yakin Dingin Di Hapus?',
    text: "Klik Ya",
    type: 'danger',
    confirmButtonText: "Iya",
    cancelButtonText: "Tidak",
    showCancelButton: true,
  })
  .then((result) => {
    if (result.value) {
      $('#formhapus'+id).remove();
      $.ajax({
        url:  BASE_URL + 'AdminController/HapusInstruksi',
        type: 'POST',
        dataType: 'json',
        data: {id_jp: id},
        success:function (berhasil) {
          if (berhasil.success) {
            Swal.fire(
              'Berhasil!',
              ''+berhasil.message+'',
              'success'
              )
          }else{
            Swal.fire(
              'Gagal!',
              ''+berhasil.message+'',
              'danger'
              )
          }
        },error:function(error) {
          Swal.fire(
            'Error!',
            'Muat Ulang',
            'info'
            )
        }
      })
    }
  })
}
</script>


