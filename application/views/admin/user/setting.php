<style type="text/css">
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 32px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 33px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    background-color:transparent;     
    padding: 0 0 0 10px;
  }
  .select2-container .select2-selection--single {
    height: 35px;
  }
  .select2-container {
    display:block;
    width:100% !important; 
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #fff transparent transparent transparent !important;
  }
</style>
<div class="row">
  <div class="card card-tasks">
    <div class="col-md-12">
      <div class="card-body">
       <form id="edituser" action="javascript:void(0);" method="post">
       	<p style="color: red;">*Perhatian Jika Tidak Ingin Mengubah Password Silakan Di Kosongkan Saja</p>
        <hr>
        <center><h3>Pengaturan Akun</h3></center>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="inputEmail4">Nama Akun</label>
            <input type="text" class="form-control"  value="<?= $this->session->userdata('nama'); ?>" autocomplete="off" name="namaakun" placeholder="Nama Akun">
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress">Username</label>
          <input type="text" class="form-control" value="<?= $this->session->userdata('username'); ?>" autocomplete="off" name="username" placeholder="Masukan Username Login">
        </div>
        <div class="form-group">
          <label for="inputAddress">Nomor Handphone</label>
          <input type="text" class="form-control" value="<?= $this->session->userdata('nohp'); ?>" autocomplete="off" name="nohp" placeholder="Masukan Nomor Hp">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Password</label>
          <input type="password" autocomplete="off" name="pass" class="form-control" placeholder="Masukan Password">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Ulang Password</label>
          <input type="password" autocomplete="off" name="re_pass" class="form-control" placeholder="Masukan Ulang Password">
        </div>
        <button type="submit" class="btn btn-primary text-white">Ubah</button>
      </form>
      </div>
    </div>
  </div>
</div>