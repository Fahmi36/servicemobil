<style type="text/css">
  .select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 32px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 33px;
  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
    background-color:transparent;     
    padding: 0 0 0 10px;
  }
  .select2-container .select2-selection--single {
    height: 35px;
  }
  .select2-container {
    display:block;
    width:100% !important; 
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #fff transparent transparent transparent !important;
  }
</style>
<div class="row">
  <div class="card card-tasks">
    <div class="col-md-12">
      <div class="card-header">
        <h4>Daftar Work Order </h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped dataTable"><a class="btn btn-primary text-white" onclick="modalForm()">Tambah</a>
            <thead>
              <th>Nomor Work Order</th>
              <th>Nama Pelanggan</th>
              <th>No Polisi</th>
              <th>Status Pekerjaan</th>
              <th>Tanggal Di Terima</th>
              <th>Tindakan</th>
            </thead>
            <tbody id="contentNya">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Input Data WO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="datawo" action="javascript:void(0);" method="post">
          <hr>
          <center><h3>Info Pelanggan</h3></center>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Nama Pemilik</label>
              <input type="text" class="form-control" id="pemilik" autocomplete="off" name="namapemilik" placeholder="Nama Pemilik">
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress">Nama STNK</label>
            <input type="text" class="form-control" id="stnk" autocomplete="off" name="stnk" placeholder="Nama STNK">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Alamat</label>
            <textarea autocomplete="off" id="alamat" name="alamat" class="form-control" placeholder="Masukan Alamat" colspan="5" autocomplete="off"></textarea>
          </div>
          <div class="form-group">
            <label for="inputAddress2">Telepon</label>
            <input type="number" autocomplete="off" name="telpon" class="form-control" id="telpon" placeholder="Nomor Telpon">
          </div>
          <div class="form-group">
            <label for="inputAddress2">No NPWP</label>
            <input type="text" class="form-control" autocomplete="off" id="npwp" name="npwp" placeholder="Nomor NPWP">
          </div>
          <hr>
          <center><h3>Info Kendaraan</h3></center>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Merk Kendaraan</label>
              <input type="text" class="form-control" id="merk" autocomplete="off" name="mrk" placeholder="Nama Kendaraan">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">No Polisi</label>
              <input type="text" class="form-control" id="nopol" autocomplete="off" name="nopolisi" placeholder="Nama Pemilik">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="inputCity">No Rangka</label>
              <input type="text" class="form-control" id="norangka" autocomplete="off" name="norangka" placeholder="Nomor Rangka">
            </div>
            <div class="form-group col-md-6">
              <label for="inputState">No Mesin</label>
              <input type="text" class="form-control" id="nomesin" autocomplete="off" name="no_mesin" placeholder="Masukan Nomor Mesin">
            </div>
            <div class="form-group col-md-6">
              <label for="inputZip">Model</label>
              <input type="text" class="form-control" id="model" autocomplete="off" name="model" placeholder="Masukan Model Mobil">
            </div>
            <div class="form-group col-md-6">
              <label for="inputZip">Type</label>
              <input type="text" class="form-control" id="type" autocomplete="off" name="type" placeholder="Masukan Type Mobile">
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress2">Warna</label>
            <input type="text" class="form-control" id="warna" autocomplete="off" name="warna" placeholder="Masukan Warna Mobil">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Bahan Bakar</label>
            <input type="text" autocomplete="off" id="bbm" name="bbm" class="form-control" placeholder="Masukan Bahan Bakar">
          </div>
          <div class="form-group">
            <label for="inputAddress2">Tanggal DEC</label>
            <input type="date" class="form-control" id="tgl_beli" autocomplete="off" name="tglbelimobil" placeholder="Masukan Tanggal DEC">
          </div>
          <hr>
          <center><h3>Order Pekerjaan</h3></center>
          <div class="form-group">
            <label for="inputAddress2">Keluhan Pelanggan</label>
            <textarea autocomplete="off" name="keluhan" id="keluhan" class="form-control" placeholder="Masukan Keluhan Pelanggan" colspan="5" autocomplete="off"></textarea>
          </div>
          <div class="form-group">
            <label for="inputAddress2">Sparepart</label>
            <input type="text" autocomplete="off" name="sparepart" class="form-control" placeholder="Masukan Nama Part Jika Di butuhkan">
          </div>
          <hr>
          <center><h3>Jadwal</h3></center>
          <div class="form-row">
            <div class="form-group col-md-12">
              <label for="inputEmail4">Tanggal Penerimaan</label>
              <input type="date" class="form-control" id="tgl_terima" autocomplete="off" name="tgl_terima" placeholder="Masukan Tanggal Penerimaan">
            </div>
          </div>
          <div class="form-group">
            <label for="inputAddress">Estimasi Waktu Service</label>
            <input type="number" class="form-control" id="tgl_service" autocomplete="off" name="tgl_service" placeholder="Masukan Estimasi Service">
          </div>
<!--           <div class="form-group">
            <label for="inputAddress2">Estimasi Tanggal Selesai</label>
            <input type="date" class="form-control" autocomplete="off" name="tgl_selesai" id="tgl_selesai" placeholder="Masukan Estimasi Tanggal Selesai">
          </div> -->
          <hr>
          <div class="form-group">
            <label for="inputAddress2">Estimasi Biaya</label>
            <input type="number" class="form-control" autocomplete="off" id="estimasi" name="estimasi" placeholder="Masukan Estimasi Biaya">
          </div>
          
          <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="modal fade bd-example-modal-lg" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="EditModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="EditModalTitle">Edit Data WO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form id="editdatawo" action="javascript:void(0);" method="post">
        <hr>
        <center><h3>Info Pelanggan</h3></center>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="inputEmail4">Nama Pemilik</label>
            <input type="text" class="form-control" id="editpemilik" autocomplete="off" name="namapemilik" placeholder="Nama Pemilik">
            <input type="hidden" class="form-control" id="idwonya" autocomplete="off" name="id">
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress">Nama STNK</label>
          <input type="text" class="form-control" id="editstnk" autocomplete="off" name="stnk" placeholder="Nama STNK">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Alamat</label>
          <textarea autocomplete="off" id="editalamat" name="alamat" class="form-control" placeholder="Masukan Alamat" colspan="5" autocomplete="off"></textarea>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Telepon</label>
          <input type="number" autocomplete="off" name="telpon" class="form-control" id="edittelpon" placeholder="Nomor Telpon">
        </div>
        <div class="form-group">
          <label for="inputAddress2">No NPWP</label>
          <input type="text" class="form-control" autocomplete="off" id="editnpwp" name="npwp" placeholder="Nomor NPWP">
        </div>
        <hr>
        <center><h3>Info Kendaraan</h3></center>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="inputEmail4">Merk Kendaraan</label>
            <input type="text" class="form-control" id="editmerk" autocomplete="off" name="mrk" placeholder="Nama Kendaraan">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="inputEmail4">No Polisi</label>
            <input type="text" class="form-control" id="editnopol" autocomplete="off" name="nopolisi" placeholder="Nama Pemilik">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCity">No Rangka</label>
            <input type="text" class="form-control" id="editnorangka" autocomplete="off" name="norangka" placeholder="Nomor Rangka">
          </div>
          <div class="form-group col-md-6">
            <label for="inputState">No Mesin</label>
            <input type="text" class="form-control" id="editnomesin" autocomplete="off" name="no_mesin" placeholder="Masukan Nomor Mesin">
          </div>
          <div class="form-group col-md-6">
            <label for="inputZip">Model</label>
            <input type="text" class="form-control" id="editmodel" autocomplete="off" name="model" placeholder="Masukan Model Mobil">
          </div>
          <div class="form-group col-md-6">
            <label for="inputZip">Type</label>
            <input type="text" class="form-control" id="edittype" autocomplete="off" name="type" placeholder="Masukan Type Mobile">
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress2">Warna</label>
          <input type="text" class="form-control" id="editwarna" autocomplete="off" name="warna" placeholder="Masukan Warna Mobil">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Bahan Bakar</label>
          <input type="text" autocomplete="off" id="editbbm" name="bbm" class="form-control" placeholder="Masukan Bahan Bakar">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Tanggal DEC</label>
          <input type="date" class="form-control" id="edittgl_beli" autocomplete="off" name="tglbelimobil" placeholder="Masukan Tanggal DEC">
        </div>
        <hr>
        <center><h3>Order Pekerjaan</h3></center>
        <div class="form-group">
            <label for="inputAddress2">Keluhan Pelanggan</label>
            <textarea autocomplete="off" name="keluhan" id="editkeluhan" class="form-control" placeholder="Masukan Keluhan Pelanggan" colspan="5" autocomplete="off"></textarea>
          </div>
        <div class="form-group">
          <label for="inputAddress2">Sparepart</label>
          <input type="text" autocomplete="off" name="sparepart" id="editpart" class="form-control" placeholder="Masukan Nama Part Jika Di butuhkan">
        </div>
        <hr>
        <center><h3>Jadwal</h3></center>
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="inputEmail4">Tanggal Penerimaan</label>
            <input type="date" class="form-control" id="edittgl_terima" autocomplete="off" name="tgl_terima" placeholder="Masukan Tanggal Penerimaan">
          </div>
        </div>
        <div class="form-group">
          <label for="inputAddress">Estimasi Tanggal Service</label>
          <input type="number" class="form-control" id="edittgl_service" autocomplete="off" name="tgl_service" placeholder="Masukan Estimasi Service">
        </div>
        <!-- <div class="form-group">
          <label for="inputAddress2">Estimasi Tanggal Selesai</label>
          <input type="date" class="form-control" autocomplete="off" name="tgl_selesai" id="edittgl_selesai" placeholder="Masukan Estimasi Tanggal Selesai">
        </div> -->
        <hr>
        <div class="form-group">
          <label for="inputAddress2">Estimasi Biaya</label>
          <input type="number" class="form-control" autocomplete="off" id="editestimasi" name="estimasi" placeholder="Masukan Estimasi Biaya">
        </div>

        <button type="submit" class="btn btn-primary">Kirim</button>
      </form>
    </div>
  </div>
</div>
</div>
<div class="modal fade bd-example-modal-lg" id="LihatModal" tabindex="-1" role="dialog" aria-labelledby="LihatModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="LihatModalTitle">Lihat Data WO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editdatawo" action="javascript:void(0);" method="post">
          <hr>
          <center><h3>Info Pelanggan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Nama Pemilik</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmpemiliktext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Nama di STNK</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="namastnktext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Alamat</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="alamattext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Telepon</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="telptext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No NPWP</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nonpwptext"></label>
              </div>
            </div>
          </div>
          <hr>
          <center><h3>Info Kendaraan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Merk Kendaraan</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="merkkentext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Polisi</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nopolisitext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Rangka</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="norangkatext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>No Mesin</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nomesintext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Model</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="modelkentext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Type</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="typekenttext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Warna</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="warnatext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Bahan Bakar</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="typetext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Tanggal DEC</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;"> 
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="tglbelitext"></label>
              </div>
            </div>
          </div>
          <hr>
          <center><h3>Order Pekerjaan</h3></center>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Keluhan Pelanggan</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="keluhantext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Spare Part</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="nmparttext"></label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-md-3">
                <label>Pembuat data Work Order</label>
              </div>
              <div class="col-md-1" style="max-width: 3.33333%;">
                <label>:</label>
              </div>
              <div class="col-md-7">
                <label id="namauserttext"></label>
              </div>
            </div>
          </div>
          <hr>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
  jQuery(document).ready(function($) {
    wp.dataALLWO();
  });
  function modalForm() {
    $("#exampleModalLong").modal({backdrop:'static',keyboard:false});
  }
  function LihatWO(id) {
    $.ajax({
      url: BASE_URL + 'AdminController/getDataWO',
      type: 'post',
      dataType: 'json',
      data: {id: id},
      success:function (data) {
        if (data.success) {
          $('#alamattext').text(data.data[0].alamat);
          $('#nmparttext').text(data.data[0].part);
          $('#estimasitext').text(data.data[0].estimasi_biaya);
          $('#merkkentext').text(data.data[0].merk_kendaraan);
          $('#modelkentext').text(data.data[0].model_kendaraan);
          $('#namastnktext').text(data.data[0].nama_stnk);
          $('#keluhantext').text(data.data[0].keluhan);
          $('#nmpemiliktext').text(data.data[0].nm_pemilik);
          $('#nomesintext').text(data.data[0].no_mesin);
          $('#nonpwptext').text(data.data[0].no_npwp);
          $('#nopolisitext').text(data.data[0].no_polisi);
          $('#norangkatext').text(data.data[0].no_rangka);
          $('#nowotext').text(data.data[0].no_wo);
          $('#stalltext').text(data.data[0].stall);
          $('#statuskerjatext').text(data.data[0].status_kerja);
          $('#telptext').text(data.data[0].telepon);
          $('#tglbelitext').text(data.data[0].tgl_pembelian);
          $('#tglselesaitext').text(data.data[0].tgl_selesai);
          $('#tglterimatext').text(data.data[0].tgl_terima);
          $('#typetext').text(data.data[0].type_bbm);
          $('#typekenttext').text(data.data[0].type_kendaraan);
          $('#wmulaitext').text(data.data[0].w_mulai);
          $('#wprosestext').text(data.data[0].w_proses);
          $('#wtunggutext').text(data.data[0].w_tunggu);
          $('#warnatext').text(data.data[0].warna);
          $('#namauserttext').text(data.data[0].namauser);
          $("#LihatModal").modal({backdrop:'static',keyboard:false});
          
        }else{
          Swal.fire(
            'Error!',
            'Silakan Klik Ulang',
            'info'
            )
        }
      },error:function(error) {
        Swal.fire(
          'Error!',
          'Silakan Klik Ulang',
          'info'
          )
      }
    })
  }
  function EditWO(id) {
   $.ajax({
    url: BASE_URL + 'AdminController/getDataWO',
    type: 'post',
    dataType: 'json',
    data: {id: id},
    success:function (data) {
      if (data.success) {
        $('#idwonya').val(data.data[0].href);
        $('#editpemilik').val(data.data[0].nm_pemilik);
        $('#editstnk').val(data.data[0].nama_stnk);
        $('#editalamat').val(data.data[0].alamat);
        $('#edittelpon').val(data.data[0].telepon);
        $('#editnpwp').val(data.data[0].no_npwp);
        $('#editmerk').val(data.data[0].merk_kendaraan);
        $('#editnopol').val(data.data[0].no_polisi);
        $('#editnorangka').val(data.data[0].no_rangka);
        $('#editnomesin').val(data.data[0].no_mesin);
        $('#editmodel').val(data.data[0].model_kendaraan);
        $('#edittype').val(data.data[0].type_kendaraan);
        $('#editwarna').val(data.data[0].warna);
        $('#editbbm').val(data.data[0].type_bbm);
        $('#edittgl_beli').val(data.data[0].tgl_pembelian);
        $('#editkeluhan').val(data.data[0].keluhan);
        $('#editpart').val(data.data[0].part);
        $('#edittgl_terima').val(data.data[0].tgl_terima);
        $('#edittgl_service').val(data.data[0].w_service);
        $('#edittgl_selesai').val(data.data[0].tgl_selesai);
        $('#editestimasi').val(data.data[0].estimasi_biaya);
        $("#EditModal").modal({backdrop:'static',keyboard:false});
      }else{
       Swal.fire(
        'Error!',
        'Silakan Klik Ulang',
        'info'
        )
     }
   },error:function(error) {
    Swal.fire(
      'Error!',
      'Silakan Klik Ulang',
      'info'
      )
  }
})
 }
 function DeleteWO(id) {
  Swal.fire({
    title: 'Yakin Ingin Hapus data ?',
    text: "Klik Ya",
    type: 'success',
    confirmButtonText: "Iya",
    cancelButtonText: "Belum",
    showCancelButton: true,
  })
  .then((result) => {
    if (result.value) {
     $.ajax({
      url: BASE_URL + 'AdminController/deleteWO',
      type: 'post',
      dataType: 'json',
      data: {id: id},
      success:function (data) {
        if (data.code == 200) {
         Swal.fire(
          'Berhasil!',
          'Berhasil Hapus Data',
          'success'
          );
         wp.dataALLWO();
       }else{
        Swal.fire(
          'Error!',
          'Silakan Klik Ulang',
          'info'
          )
      }
    },error:function(error) {
      Swal.fire(
        'Error!',
        'Silakan Klik Ulang',
        'info'
        )
    }
  })
   }
 })
}
</script>