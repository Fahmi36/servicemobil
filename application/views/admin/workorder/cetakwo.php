<html>
<head>
	<title>Cetak Data WO</title>
</head>
<body>
	<div style="word-break:break-all;word-wrap:break-word;">
		<div style="text-align:right; position:absolute;">
			<table align="left" border="0" cellpadding="0" cellspacing="0" style=";width:50%;">
				<tbody style="vertical-align:top;">
					<tr>
						<td style="width: 100%;text-align:left;"><img src="<?=FCPATH.'assets/img/LogoHeader.png'?>" width="250" height="150"></td>
					</tr>
					<!--<tr>-->
					<!--	<td>PT. Tunas Ridean Tbk - Tunas Toyota Radin Inten</td>-->
					<!--</tr>-->
					<!--<tr>-->
					<!--	<td>Jl. Radin Inten 2 No.62 , Jakarta Timur</td>-->
					<!--</tr>-->
					<tr>
						<td style="text-align:left;">Ketentuan :</td>
					</tr>
					<tr>
						<td style="text-align:left;font-size:10pt;">1. WO Ini Merupakan SURAT KUASA dari pelanggan kepada PT. Tunas Ridean Tbk Untuk :</td>
					</tr>
					<tr>
						<td style="text-align:left;font-size:10pt;">2. Jaminan Pekerjaan Berlaku untuk Pekerjaan General Repair adalah 15 hari atau 1000KM mana yang tercapai lebih dulu</td>
					</tr>
					<tr>
						<td style="text-align:left;font-size:10pt;">3. Apabila dalam waktu 2 hari part bekas tidak di ambil, maka kami akan memusnahkan part tersebut</td>
					</tr>
					<tr>
						<td style="text-align:left;font-size:10pt;">4. Apabila dalam waktu 7 hari (Setelah Selesai Service) Tidak di Ambil Maka akan Kami Kenakan Denda Rp.10.000 per-hari dan apabila kendaraan mengalami rusak karena cuaca maka bukan tanggung jawab kami.</td>
					</tr>
					<tr>
						<td style="text-align:center;"><h3>WORK ORDER</h3></td>
					</tr>
				</tbody>
			</table>
			<table align="right" border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:50%;">
				<thead>
					<tr>
						<th style="text-align:center;"></th>
						<th style="text-align:center;"><?=$data->no_polisi?></th>
					</tr>
				</thead>
				<tbody style="vertical-align:top;">
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">No. WO : <?=$data->no_wo?></td>
						<td style="width: 50%;text-align:left;font-size:10pt;">Tgl DEC : <?=$data->tgl_pembelian?></td>
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Tgl / Jam : <?=date('Y-m-d H:i:s')?></td>
						<td style="width: 50%;text-align:left;font-size:10pt;">Tipe Bahan Bakar : <?=$data->type_bbm?></td>
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Tipe Model : <?=$data->type_kendaraan?></td>
						<td rowspan="7" style="height:100px;width: 50%;text-align:left;font-size:10pt;">Alamat Pemilik : <?=$data->alamat?></td>
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Warna : <?=$data->warna?></td>
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Tgl Penerimaan : <?=date('d F Y',strtotime($data->tgl_terima))?></td>	
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Tgl Penyerahan : <?=date('d F Y',strtotime($data->type_kendaraan))?></td>	
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Nama Pemilik : <?=$data->nm_pemilik?></td>	
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">Telepon : <?=$data->telepon?></td>	
					</tr>
					<tr>
						<td style="width: 50%;text-align:left;font-size:10pt;">NPWP : <?=$data->no_npwp?></td>
					</tr>
				</tbody>
			</table>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<table align="left" border="1" cellpadding="0" cellspacing="0" style="width:50%;">
				<thead>
					<tr>
						<th style="text-align:center;">Keluhan</th>
					</tr>
				</thead>
				<tbody style="vertical-align:top;">
					<tr>
						<td style="height:150px;width: 50%;text-align:left;font-size:14pt;"><?=$data->keluhan?></td>
					</tr>
				</tbody>
			</table>
			<table align="right" border="1" cellpadding="0" cellspacing="0" style="width:50%;">
				<thead>
					<tr>
						<th style="text-align:center;">Order Pekerjaan</th>
						<th style="text-align:center;">Estimasi Biaya</th>
					</tr>
				</thead>
				<tbody style="vertical-align:top;">
					<tr>
						<td style="height:150px;width: 50%;text-align:left;font-size:14pt;">
							<?php foreach ($kerja as $key): ?>
								<?=$key->nm_pekerjaan?>
							<?php endforeach ?>
						</td>
						<td style="height:150px;width: 50%;text-align:left;font-size:14pt;"><?=$data->estimasi_biaya?></td>
					</tr>
				</tbody>
			</table>

			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<table align="right" border="1" cellpadding="0" cellspacing="0" style="width:50%;">
				<tbody style="vertical-align:top;">
					<tr>
						<td style="width: 50%;text-align:left;">Pemilik Kendaraan</td>
						
						<td style="width: 50%;text-align:left;">Service Advisor</td>
					</tr>
					<tr>
						<td style="height:100px;width: 50%;text-align:center;"><br><br><br><br><br><?=$data->nm_pemilik?><br><br></td>
						<td style="height:100px;width: 50%;text-align:center;"><br><br><br><br><br><?=$data->namauser?><br><br></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>