<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndexController extends CI_Controller {

	public function index()
	{
		if ($this->session->userdata('id') != null) {
			redirect('admin/dashboard');
		}else{
			$data['title'] = 'Selamat Datang Di Halaman Login PKK';
			$data['link_view'] = 'login';
			$data['show_pg_head'] = 'none';
			$this->load->view('utama',$data);
		}
	}
	function actionlogin()
	{
		$no = $this->input->post('username');
		if (empty($no)) {
			$val = array('success'=>false,'msg'=>'Silakan Masukan username');
		}else{
			$this->db->select('users.*');
			$this->db->from('users');
			$this->db->where('users.aktif !=',0);
			$this->db->where('users.username', $no);
			$cek = $this->db->get();
			$row = $cek->row();
			if ($cek->num_rows() > 0 ) {
				$cekpassanggota = password_verify(''.$this->input->post('password').'', ''.$row->password.'');
				if ($cekpassanggota == true) {
					$session = array('id'=>$row->id,'username' => $row->username,'nama' => $row->name,'role'=>$row->role);
					$this->session->set_userdata($session);
					$val = array('success'=>true,'msg'=>'success');
				}else{
					$val = array('success'=>false,'msg'=>'Username Atau Password Salah');
				}
			}else{
				$val = array('success'=>false,'msg'=>'data tidak ada');
			}
		}
		echo json_encode($val);
	}
}

/* End of file IndexController.php */
/* Location: ./application/controllers/IndexController.php */