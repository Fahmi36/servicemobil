<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class AdminController extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
	}
	function dashboard()
	{
		if ($this->session->userdata('id') != null) {
			$this->db->select('sum(w_tunggu) / count(*) as rerata_t, sum(al_waktu) / count(*) as rerata_s, sum(w_proses) / sum(al_waktu) as utilitas, sum(al_waktu) / sum(w_proses / 8) as job');
			$this->db->from('penjadwalan');
			$q = $this->db->get();
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['status'] = $q;
			$data['link_view'] = 'admin/dashboard';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function printPdf($id='')
	{
		$id= $this->uri->segment(2);
		if ($id==null) {
			redirect('/');
		}else{
			if ($this->session->userdata('id') != null) {
				$this->load->library('pdf');
				$this->db->select('work_order.*,pelanggan.*,data_kendaraan.*,penjadwalan.*,md5(work_order.id) as href,status_kerja.nama as status_kerja,users.name as namauser');
				$this->db->from('work_order');
				$this->db->join('pelanggan', 'pelanggan.id_wo = work_order.id', 'INNER');
				$this->db->join('data_kendaraan', 'data_kendaraan.id_wo = work_order.id', 'INNER');
				$this->db->join('penjadwalan', 'penjadwalan.id_wo = work_order.id', 'INNER');
				$this->db->join('status_kerja', 'status_kerja.id_status = work_order.status_pekerjaan', 'INNER');
				$this->db->join('users', 'users.id = work_order.created_by', 'INNER');
				$this->db->where('md5(work_order.id)', $id);
				$q = $this->db->get();
				if ($q->num_rows() > 0) {
					$this->db->select('nm_pekerjaan');
					$this->db->from('jenis_pekerjaan');
					$this->db->where('md5(id_wo)', $id);
					$kerja = $this->db->get();
					$data['data'] = $q->row();
					$data['kerja'] = $kerja->result();
					$this->pdf->setPaper('A4', 'potrait');
					$this->pdf->filename = "PrintWo".@$data['data']->no_wo.".pdf";
					$this->pdf->load_view('admin/workorder/cetakwo', $data);
				}else{
					redirect('/');
				}
			}else{
				redirect('/');
			}
		}
	}
	function DataRataRata()
	{
		$this->db->select('sum(w_tunggu) / count(*) as rerata_t, sum(al_waktu) / count(*) as rerata_s, sum(w_proses) / sum(al_waktu) as utilitas, sum(al_waktu)/sum(w_proses) as job');
		$this->db->from('penjadwalan');
		$q = $this->db->get();
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Halaman Input Data Work Order';
			$data['status'] = $q;
			$data['link_view'] = 'admin/rerata';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function inputdatawo()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Halaman Input Data Work Order';
			$data['status'] = $this->db->get('kategori');
			$data['link_view'] = 'admin/workorder/datawo';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}

	function getNoWO(){

		@$r = $this->db->query("SELECT MAX(id) as num FROM work_order")->row();
		@$num_padded = sprintf("%05d", $r->num + 1);
		@$tx_code    = '5Y0/WOG/'.date('ymd') . $num_padded;
		$this->session->set_userdata('sessNoPermohonan',$tx_code);
		return ucwords($tx_code);
	}
	function qDataWO()
	{
		$this->db->select('work_order.*,penjadwalan.*,data_kendaraan.no_polisi');
		$this->db->from('work_order');
		$this->db->join('penjadwalan', 'penjadwalan.id_wo = work_order.id', 'INNER');
		$this->db->join('data_kendaraan', 'data_kendaraan.id_wo = work_order.id', 'INNER');
		$this->db->where('stall !=', null);
		$q = $this->db->get();
		$data = array();
		$i = 0;
		foreach ($q->result() as $key) {
			$data[$i] = array(
				'id'=>md5($key->id_wo),
				'text'=>$key->no_polisi.'<br>'.$key->no_wo,
				'start'=>$key->w_mulai,
				'end'=>$key->tgl_selesai,
				'resource'=>$key->stall,
				'status'=>$key->status_pekerjaan,
			);
			$i++;
		}
		echo json_encode($data);
	}
	function getDataWO()
	{
		// $this->db->distinct('jenis_pekerjaan.id_wo');
		$this->db->select('work_order.*,pelanggan.*,data_kendaraan.*,penjadwalan.*,md5(work_order.id) as href,status_kerja.nama as status_kerja,users.name as namauser');
		$this->db->from('work_order');
		$this->db->join('pelanggan', 'pelanggan.id_wo = work_order.id', 'INNER');
		$this->db->join('data_kendaraan', 'data_kendaraan.id_wo = work_order.id', 'INNER');
		$this->db->join('penjadwalan', 'penjadwalan.id_wo = work_order.id', 'INNER');
		// $this->db->join('jenis_pekerjaan', 'jenis_pekerjaan.id_wo = work_order.id', 'INNER');
		$this->db->join('status_kerja', 'status_kerja.id_status = work_order.status_pekerjaan', 'INNER');
		$this->db->join('users', 'users.id = work_order.created_by', 'INNER');
		if ($this->input->post('id') != null) {
			$this->db->where('md5(work_order.id)', $this->input->post('id'));
		}
		$q = $this->db->get();
		echo json_encode(array('success'=>true,'data'=>$q->result()));
	}
	function getInstruksi()
	{
		// $this->db->distinct('jenis_pekerjaan.id_wo');
		$this->db->select('jenis_pekerjaan.*,md5(jenis_pekerjaan.id_jp) as idinstruksi');
		$this->db->from('work_order');
		$this->db->join('jenis_pekerjaan', 'jenis_pekerjaan.id_wo = work_order.id', 'INNER');
		$this->db->where('md5(work_order.id)', $this->input->post('id'));
		$q = $this->db->get();
		echo json_encode(array('success'=>true,'data'=>$q->result()));
	}
	function Adddatawo()
	{
		$response = array();
		if (!$this->input->is_ajax_request()) {
			$response = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id')==null) {
				redirect('/');
				return;
			}else{
				$this->form_validation->set_rules('namapemilik','namapemilik','required',array('required' => 'Mohon Isi nama pemilik Pelanggan'));
				$this->form_validation->set_rules('stnk','Nama stnk','required',array('required' => 'Mohon Isi Nama stnk Pelanggan'));
				$this->form_validation->set_rules('alamat','alamat','required',array('required' => 'Mohon Isi alamat Pelanggan'));
				$this->form_validation->set_rules('telpon','telpon','required',array('required' => 'Mohon Isi telpon Pelanggan'));
				$this->form_validation->set_rules('nopolisi','nopolisi','required',array('required' => 'Mohon Isi nopolisi Pelanggan'));
				$this->form_validation->set_rules('norangka','norangka','required',array('required' => 'Mohon Isi norangka Pelanggan'));
				$this->form_validation->set_rules('no_mesin','no_mesin','required',array('required' => 'Mohon Isi no mesin Pelanggan'));
				$this->form_validation->set_rules('model','model','required',array('required' => 'Mohon Isi model Kendaraan Pelanggan'));
				$this->form_validation->set_rules('type','type','required',array('required' => 'Mohon Isi type Kendaraan Pelanggan'));
				$this->form_validation->set_rules('bbm','bbm','required',array('required' => 'Mohon Isi bbm Kendaraan Pelanggan'));
				$this->form_validation->set_rules('tgl_terima','tgl_terima','required',array('required' => 'Mohon Isi tgl terima Pelanggan'));
				if ($this->form_validation->run() == true) {
					$this->db->trans_start();
					// $part = '';
					// $partinput = $this->input->post('sparepart');
					// for ($i=0; $i <count($partinput); $i++) { 
					// 	$part .= $partinput[$i].',';
					// 	$this->db->set('jml_part','jml_part-1',FALSE);
					// 	$this->db->where('id_part', $partinput[$i]);
					// 	$this->db->update('data_part');
					// }
					$dawo = array(
						'part'=>$this->input->post('sparepart'),
						'no_wo'=>$this->getNoWO(),
						'estimasi_biaya'=>$this->input->post('estimasi'),
						'keluhan'=>$this->input->post('keluhan'),
						'status_pekerjaan'=>'1',
						'created_by'=>$this->session->userdata('id'),
						'created_at'=>date('Y-m-d H:i:s'),
					);
					$datawo = $this->db->insert('work_order', $dawo);
					$idwo = $this->db->insert_id();
					if ($datawo) {
						$dapel = array(
							'id_wo'=>$idwo,
							'nm_pemilik'=>$this->input->post('namapemilik'),
							'no_npwp'=>$this->input->post('npwp'),
							'telepon'=>$this->input->post('telpon'),
							'alamat'=>$this->input->post('alamat'),
						);
						$inputpel = $this->db->insert('pelanggan', $dapel);
						if ($inputpel) {
							$dtken = array(
								'id_wo'=>$idwo,
								'no_polisi'=>$this->input->post('nopolisi'),
								'nama_stnk'=>$this->input->post('stnk'),
								'no_rangka'=>$this->input->post('norangka'),
								'warna'=>$this->input->post('warna'),
								'model_kendaraan'=>$this->input->post('model'),
								'merk_kendaraan'=>$this->input->post('mrk'),
								'type_kendaraan'=>$this->input->post('type'),
								'no_mesin'=>$this->input->post('no_mesin'),
								'type_bbm'=>$this->input->post('bbm'),
								'tgl_pembelian'=>$this->input->post('tglbelimobil'),
								'created_at'=>date('Y-m-d H:i:s'),
							);
							$inputken = $this->db->insert('data_kendaraan', $dtken);
							if ($inputken) {
								$dtjadwal = array(
									'id_wo'=>$idwo,
									'tgl_terima'=>date('Y-m-d',strtotime($this->input->post('tgl_terima'))),
									// 'tgl_selesai'=>date('Y-m-d',strtotime($this->input->post('tgl_selesai'))),
									'w_service'=>$this->input->post('tgl_service'),
									'created_at'=>date('Y-m-d H:i:s'),
								);
								$inputjadwal = $this->db->insert('penjadwalan', $dtjadwal);
								if ($inputjadwal) {
									// $dtkerja = array(
									// 	'id_wo'=>$idwo,
									// 	// 'nm_pekerjaan'=>$this->input->post('instruksi'),
									// 	'created_at'=>date('Y-m-d H:i:s'),
									// );
									// $inputkerja = $this->db->insert('jenis_pekerjaan', $dtkerja);
									// if ($inputkerja) {
									$this->db->trans_commit();
									$response['code'] = 200;
									$response['message'] = 'Berhasil Menambahkan Data Work Order';
									// }else{
									// 	$this->db->trans_rollback();
									// 	$response['code'] = 400;
									// 	$response['message'] = 'Gagal Insert Jenis Pekerjaan';
									// }
								}else{
									$this->db->trans_rollback();
									$response['code'] = 400;
									$response['message'] = 'Gagal Insert Jadwal Pekerjaan';
								}
							}else{
								$this->db->trans_rollback();
								$response['code'] = 400;
								$response['message'] = 'Gagal Insert data Kendaraan';
							}
						}else{
							$this->db->trans_rollback();
							$response['code'] = 400;
							$response['message'] = 'Gagal Insert Data Pelanggan';
						}
					}else{
						$this->db->trans_rollback();
						$response['code'] = 400;
						$response['message'] = 'Gagal Insert Data WO';
					}
				}else{
					$this->db->trans_rollback();
					$response['code'] = 400;
					$response['message'] = validation_errors();
				}
			}
		}
		echo json_encode($response);
	}
	function Editdatawo()
	{
		$response = array();
		if (!$this->input->is_ajax_request()) {
			$response = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id')==null) {
				redirect('/');
				return;
			}else{
				$this->form_validation->set_rules('namapemilik','namapemilik','required',array('required' => 'Mohon Isi nama pemilik Pelanggan'));
				$this->form_validation->set_rules('stnk','Nama stnk','required',array('required' => 'Mohon Isi Nama stnk Pelanggan'));
				$this->form_validation->set_rules('alamat','alamat','required',array('required' => 'Mohon Isi alamat Pelanggan'));
				$this->form_validation->set_rules('telpon','telpon','required',array('required' => 'Mohon Isi telpon Pelanggan'));
				$this->form_validation->set_rules('nopolisi','nopolisi','required',array('required' => 'Mohon Isi nopolisi Pelanggan'));
				$this->form_validation->set_rules('norangka','norangka','required',array('required' => 'Mohon Isi norangka Pelanggan'));
				$this->form_validation->set_rules('no_mesin','no_mesin','required',array('required' => 'Mohon Isi no mesin Pelanggan'));
				$this->form_validation->set_rules('model','model','required',array('required' => 'Mohon Isi model Kendaraan Pelanggan'));
				$this->form_validation->set_rules('type','type','required',array('required' => 'Mohon Isi type Kendaraan Pelanggan'));
				$this->form_validation->set_rules('bbm','bbm','required',array('required' => 'Mohon Isi bbm Kendaraan Pelanggan'));
				$this->form_validation->set_rules('tgl_terima','tgl_terima','required',array('required' => 'Mohon Isi tgl terima Pelanggan'));
				if ($this->form_validation->run() == true) {
					$this->db->trans_start();
					$this->db->select('*');
					$this->db->from('work_order');
					$this->db->where('md5(work_order.id)', $this->input->post('id'));
					$querydata = $this->db->get();
					if ($querydata->num_rows() > 0) {
						$row = $querydata->row();
						// $cek_part = $this->db->get_where('histori_part', array('id_wo'=>$row->id));
						// $part = '';
						// $partinput = $this->input->post('sparepart');
						// if ($cek_part->num_rows() > 0) {
						// 	if (count($partinput) > 0) {
						// 		$this->db->get_where('', limit, offset);
						// 	}
						// }else{
						// 	if (count($partinput) > 0) {
						// 		# code...
						// 	}else{
						// 		@$this->db->delete('histori_part',array('id_wo'=>$row->id));
						// 	}
						// }
						// $part = '';
						// $partinput = $this->input->post('sparepart');
						// for ($i=0; $i <count($partinput); $i++) { 
						// 	$part .= $partinput[$i].',';
						// 	$this->db->set('jml_part','jml_part-1',FALSE);
						// 	$this->db->where('id_part', $partinput[$i]);
						// 	$this->db->update('data_part');
						// }
						$dawo = array(
							'part'=>$this->input->post('sparepart'),
							'no_wo'=>$this->getNoWO(),
							'estimasi_biaya'=>$this->input->post('estimasi'),
							'keluhan'=>$this->input->post('keluhan'),
							'updated_by'=>$this->session->userdata('id'),
							'created_at'=>date('Y-m-d H:i:s'),
						);
						$wherewo = array(
							'id'=>$row->id,
						);
						$datawo = $this->db->update('work_order', $dawo,$wherewo);
						if ($datawo) {
							$dapel = array(
								'nm_pemilik'=>$this->input->post('namapemilik'),
								'no_npwp'=>$this->input->post('npwp'),
								'telepon'=>$this->input->post('telpon'),
								'alamat'=>$this->input->post('alamat'),
							);
							$wherepel = array(
								'id_wo'=>$row->id,
							);
							$inputpel = $this->db->update('pelanggan', $dapel,$wherepel);
							if ($inputpel) {
								$dtken = array(
									'no_polisi'=>$this->input->post('nopolisi'),
									'nama_stnk'=>$this->input->post('stnk'),
									'no_rangka'=>$this->input->post('norangka'),
									'warna'=>$this->input->post('warna'),
									'model_kendaraan'=>$this->input->post('model'),
									'merk_kendaraan'=>$this->input->post('mrk'),
									'type_kendaraan'=>$this->input->post('type'),
									'no_mesin'=>$this->input->post('no_mesin'),
									'type_bbm'=>$this->input->post('bbm'),
									'tgl_pembelian'=>$this->input->post('tglbelimobil'),
									'created_at'=>date('Y-m-d H:i:s'),
								);
								$whereken = array(
									'id_wo'=>$row->id,
								);
								$inputken = $this->db->update('data_kendaraan', $dtken,$whereken);
								if ($inputken) {
									$dtjadwal = array(
										'tgl_terima'=>date('Y-m-d',strtotime($this->input->post('tgl_terima'))),
										// 'tgl_selesai'=>date('Y-m-d',strtotime($this->input->post('tgl_selesai'))),
										'w_service'=>$this->input->post('tgl_service'),
										'created_at'=>date('Y-m-d H:i:s'),
									);
									$wherejadwal = array(
										'id_wo'=>$row->id,
									);
									$inputjadwal = $this->db->update('penjadwalan', $dtjadwal,$wherejadwal);
									if ($inputjadwal) {
										// $dtkerja = array(
										// 	// 'nm_pekerjaan'=>$this->input->post('instruksi'),
										// 	'updated_at'=>date('Y-m-d H:i:s'),
										// );
										// $wherekerja = array(
										// 	'id_wo'=>$row->id,
										// );
										// $inputkerja = $this->db->update('jenis_pekerjaan', $dtkerja,$wherekerja);
										// if ($inputkerja) {
										$this->db->trans_commit();
										$response['code'] = 200;
										$response['message'] = 'Berhasil Edit Data Work Order';
										// }else{
										// 	$this->db->trans_rollback();
										// 	$response['code'] = 400;
										// 	$response['message'] = 'Gagal Edit Jenis Pekerjaan';
										// }
									}else{
										$this->db->trans_rollback();
										$response['code'] = 400;
										$response['message'] = 'Gagal Edit Jadwal Pekerjaan';
									}
								}else{
									$this->db->trans_rollback();
									$response['code'] = 400;
									$response['message'] = 'Gagal Edit data Kendaraan';
								}
							}else{
								$this->db->trans_rollback();
								$response['code'] = 400;
								$response['message'] = 'Gagal Edit Data Pelanggan';
							}
						}else{
							$this->db->trans_rollback();
							$response['code'] = 400;
							$response['message'] = 'Gagal Edit Data WO';
						}
					}else{
						$this->db->trans_rollback();
						$response['code'] = 400;
						$response['message'] = 'Data Kosong';
					}
				}else{
					$this->db->trans_rollback();
					$response['code'] = 400;
					$response['message'] = validation_errors();
				}
			}
		}
		echo json_encode($response);
	}

	function DataPartALL()
	{
		$this->db->select('data_part.*,md5(data_part.id_part) as href');
		$this->db->from('data_part');
		if ($this->input->post('id') != null) {
			$this->db->where('md5(data_part.id_part)', $this->input->post('id'));
		}
		$q = $this->db->get();
		echo json_encode(array('success'=>true,'data'=>$q->result()));
	}
	function Adddatapart()
	{
		$response = array();
		if (!$this->input->is_ajax_request()) {
			$response = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id') != null) {
				@$r = $this->db->query("SELECT MAX(id_part) as num FROM data_part")->row();
				@$num_padded = sprintf("%05d", $r->num + 1);
				@$tx_code    = 'P'.$num_padded;
				$part = array(
					'kode_part'=>@$tx_code,
					'nm_part'=>$this->input->post('namapart'),
					'jml_part'=>$this->input->post('jmlpart'),
					'created_at'=>date('Y-m-d H:i:s'),
				);
				$q = $this->db->insert('data_part', $part);
				if ($q) {
					$this->db->trans_commit();
					$response['code'] = 200;
					$response['message'] = 'Berhasil Menambahkan Data Part';
				}else{
					$this->db->trans_rollback();
					$response['code'] = 400;
					$response['message'] = 'Gagal Insert Data Part';
				}
			}else{
				$this->db->trans_rollback();
				$response['code'] = 400;
				$response['message'] = 'Wajib Login Kembali';
			}
		}
		echo json_encode($response);
	}
	function Editdatapart()
	{
		$response = array();
		if (!$this->input->is_ajax_request()) {
			$response = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id') != null) {
				$part = array(
					'nm_part'=>$this->input->post('part'),
					'jml_part'=>$this->input->post('jmlart'),
					'updated_at'=>date('Y-m-d H:i:s'),
				);
				$where = array('
					md5(id_part)'=>$this->input->post('idpart'),
				);
				$q = $this->db->update('data_part', $part,$where);
				if ($q) {
					$this->db->trans_commit();
					$response['code'] = 200;
					$response['message'] = 'Berhasil Mengubah Data Part';
				}else{
					$this->db->trans_rollback();
					$response['code'] = 400;
					$response['message'] = 'Gagal Mengubah Data Part';
				}
			}else{
				$this->db->trans_rollback();
				$response['code'] = 400;
				$response['message'] = 'Wajib Login Kembali';
			}
		}
		echo json_encode($response);
	}
	function data_part()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['link_view'] = 'admin/part/datapart';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function DataPartHistory()
	{
		$this->db->select('data_part.nm_part,histori_part.jml,md5(histori_part.id_beli) as href');
		$this->db->from('data_part');
		$this->db->join('histori_part', 'histori_part.id_part = data_part.id_part', 'INNER');
		$this->db->where('md5(histori_part.id_beli)', $this->input->post('id'));
		$q = $this->db->get();
		echo json_encode(array('success'=>true,'data'=>$q->result()));
	}
	function PesananPart()
	{
		if ($this->session->userdata('id') != null) {
			$this->db->select('histori_part.status,histori_part.jml,md5(histori_part.id_beli) as href,data_part.nm_part,work_order.no_wo');
			$this->db->from('histori_part');
			$this->db->join('data_part', 'data_part.id_part = histori_part.id_part', 'INNER');
			$this->db->join('work_order', 'work_order.id = histori_part.id_wo', 'INNER');
			$this->db->order_by('histori_part.created_at', 'desc');
			$q=$this->db->get();
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['data'] = $q;
			$data['link_view'] = 'admin/part/datapesanpart';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function UbahPart()
	{
		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'success'=>false,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->input->post('idpesan') == NULL) {
				$cek = $this->db->get_where('histori_part', array('md5(id_beli)'=>$this->input->post('id')));
				$q = $this->db->update('histori_part', array('status'=>2),array('md5(id_beli)'=>$this->input->post('id')));
				if ($q) {
					$this->db->set('jml_part','jml_part-'.$cek->row()->jml,FALSE);
					$this->db->where('id_part',$cek->row()->id_part);
					$up = $this->db->update('data_part');
					if ($up) {
						$json = array(
							'success'=>true,
							'message'=> 'Berhasil ACC Barang'
						);
					}else{
						$json = array(
							'success'=>false,
							'message'=> 'Tidak Boleh Akses'
						);
					}
				}else{
					$json = array(
						'success'=>false,
						'message'=> 'Tidak Boleh Akses'
					);
				}
			}else{
				$cek = $this->db->get_where('histori_part', array('md5(id_beli)'=>$this->input->post('idpesan')));
				$this->db->from('data_part');
				$this->db->where('id_part', $cek->row()->id_part);
				$part = $this->db->get();
				if ($part->num_rows() > 0) {
					$q = $this->db->update('histori_part', array('status'=>1,'jml'=>$this->input->post('jmlpart')),array('md5(id_beli)'=>$this->input->post('idpesan')));
					if ($q) {
						$json = array(
							'success'=>true,
							'message'=> 'Berhasil Pesan Barang'
						);
					}
				}else{
					$json = array(
						'success'=>false,
						'message'=> 'Stok Gudang Kurang'
					);
				}
			}
		}
		echo json_encode($json);
	}
	function DataKendaraan()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['link_view'] = 'admin/kendaraan/datakendaraan';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function DataKenAll()
	{
		$this->db->select('data_kendaraan.*,md5(data_kendaraan.id_kendaraan) as href');
		$this->db->from('data_kendaraan');
		if ($this->input->post('id') != null) {
			$this->db->where('md5(data_kendaraan.id_kendaraan)', $this->input->post('id'));
		}
		$q = $this->db->get();
		echo json_encode(array('success'=>true,'data'=>$q->result()));
	}
	function DatajenisAll()
	{
		$this->db->select('jenis_pekerjaan.*,md5(jenis_pekerjaan.id_jp) as href,kategori.jenis_kategori');
		$this->db->from('jenis_pekerjaan');
		$this->db->join('kategori', 'jenis_pekerjaan.id_kategori = kategori.id_kategori', 'left');
		if ($this->input->post('id') != null) {
			$this->db->where('md5(jenis_pekerjaan.id_jp)', $this->input->post('id'));
		}
		$q = $this->db->get();
		echo json_encode(array('success'=>true,'data'=>$q->result()));
	}
	function DataKerusakan()
	{
		if ($this->session->userdata('id') != null) {
			$data['part'] = $this->getPart();
			$data['title'] = 'Halaman Penjadwalan Work Order';
			$data['link_view'] = 'admin/kerusakan/datakerusakan';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function DataJadwalAll()
	{
		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'success'=>false,
				'data'=> 'Tidak Boleh Akses'
			);
		}else{
			$this->db->select('penjadwalan.*,md5(penjadwalan.id_jadwal) as href,work_order.no_wo,data_kendaraan.no_polisi');
			$this->db->from('penjadwalan');
			$this->db->join('work_order', 'work_order.id =penjadwalan.id_wo ', 'INNER');
			$this->db->join('data_kendaraan', 'data_kendaraan.id_wo =work_order.id', 'INNER');
			$this->db->where_in('work_order.status_pekerjaan', [1,2]);
			if ($this->input->post('id') != null) {
				$this->db->where('md5(penjadwalan.id_jadwal)', $this->input->post('id'));
			}
			$q = $this->db->get();
			$json = array('success'=>true,'data'=>$q->result());
		}
		echo json_encode($json);
	}

	function DataService()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['link_view'] = 'admin/service/dataservice';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function EditPenjadwalandanService()
	{
		$response = array();
		if (!$this->input->is_ajax_request()) {
			$response = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id')==null) {
				redirect('/');
				return;
			}else{
				$this->form_validation->set_rules('waktumulai','Waktu Mulai','required',array('required' => 'Mohon Isi Waktu Mulai'));
				if ($this->form_validation->run() == true) {
					$this->db->trans_start();
					// if ( date('Y-m-d H:i',strtotime($this->input->post('waktumulai'))) < date('Y-m-d H:i')) {
					// 	$response['code'] = 400;
					// 	$response['message'] = 'Waktu Mulai Tidak Boleh Kurang Dari Tanggal Sekarang';
					// 	echo json_encode($response);
					// 	return;
					// }
					// return var_dump(date('Y-m-d H:i:s',strtotime($this->input->post('waktumulai'))));
					$row = $this->db->get_where('work_order', array('md5(id)'=>$this->input->post('href')));
					$jml = array();
					$jmlbaru = array();
					if ($row->num_rows() > 0) {
						for ($i=0; $i < count($this->input->post('jeniskerja')); $i++) { 
							$countjenis = $this->db->get_where('jenis_pekerjaan',array('md5(id_jp)'=>@$this->input->post('id_kategori')[$i]));
							if ($countjenis->num_rows() > 0) {
								if ($this->input->post('jeniskerja')[$i] == 'Ringan') {
									$jml[$i] = '4';
								}elseif ($this->input->post('jeniskerja')[$i] == 'Sedang'){
									$jml[$i] = '16';
								}elseif ($this->input->post('jeniskerja')[$i] == 'Berat'){
									$jml[$i] = '32';
								}
								$wherekerja = array(
									'id_jp'=>$countjenis->row()->id_jp,
								);
								$datakerja = array(
									'id_kategori'=>$this->input->post('jeniskerja')[$i],
									'nm_pekerjaan'=>$this->input->post('instruksi')[$i],
									'durasi'=>$jml[$i],
								);
								$kerja = $this->db->update('jenis_pekerjaan', $datakerja,$wherekerja);
							}else{
								$this->db->trans_rollback();
								$response['code'] = 400;
								$response['message'] = 'Gagal Edit Data Kerja';
							}
						}
						for ($i=0; $i < count($this->input->post('jeniskerjabaru')); $i++) { 

							if ($this->input->post('jeniskerjabaru')[$i] == 'Ringan') {
								$jmlbaru[$i] = '4';
							}

							if ($this->input->post('jeniskerjabaru')[$i] == 'Sedang'){
								$jmlbaru[$i] = '16';
							}

							if ($this->input->post('jeniskerjabaru')[$i] == 'Berat'){
								$jmlbaru[$i] = '32';
							}

							$datakerja = array(
								'id_wo'=>@$row->row()->id,
								'id_kategori'=>$this->input->post('jeniskerjabaru')[$i],
								'nm_pekerjaan'=>$this->input->post('instruksibaru')[$i],
								'durasi'=>$jmlbaru[$i],
							);
							$kerja = $this->db->insert('jenis_pekerjaan', $datakerja);
						}
					}else{
						$this->db->trans_rollback();
						$response['code'] = 400;
						$response['message'] = 'Gagal Edit Data Kerja';
					}
						// return var_dump($kerja);
					if ($kerja) {
						for ($i=0; $i <count($this->input->post('partbaru')) ; $i++) { 
							$object = array(
								'id_wo'=>$row->row()->id,
								'id_part'=>$this->input->post('partbaru')[$i],
								'status'=>0,
								'created_at'=>date('Y-m-d H:i:s')
							);
							$this->db->insert('histori_part', $object);
						}
						// $this->db->select('*');
						// $this->db->from('penjadwalan');
						// $this->db->join('work_order', 'work_order.id = penjadwalan.id_wo', 'INNER');
						// $this->db->where('status_pekerjaan', '2');
						// $this->db->where('stall', $this->input->post('stall'));
						// $this->db->order_by('work_order.created_at', 'DESC');
						// $q = $this->db->get();

						$getstall = $this->db->get('stall');
						$datastall = array();
						foreach ($getstall->result() as $key) {
							$datastall[] = $key->nm_stall;
						}
						$stall = array();
						for ($i=0; $i < count($datastall); $i++) {
							$this->db->select('*');
							$this->db->from('penjadwalan');
							$this->db->join('work_order', 'penjadwalan.id_wo = work_order.id', 'INNER');
							$this->db->where('stall',$datastall[$i]);
							$this->db->where('status_pekerjaan', 2);
							$q = $this->db->get();
							if ($q->num_rows() > 0) {
								if ($q->row()->stall == $datastall[$i]) {
									$stall[] = null;
								}else{
									$stall[] = $datastall[$i];
								}
							}else{
								$stall[] = $datastall[$i];
							}
						}
						$hasilstall = array();
						for ($i=0; $i < count($stall); $i++) { 
							if ($stall[$i] != null) {
								$hasilstall[] = $stall[$i];
							}
						}
						// return var_dump($hasilstall);

						// if ($this->input->post('proses') != NULL AND $this->input->post('prosesbaru') != NULL) {
						// 	$proses = (array_sum($this->input->post('proses')) + array_sum($this->input->post('prosesbaru')));
						// }else if ($this->input->post('proses') == NULL AND $this->input->post('prosesbaru') != NULL) {
						// 	$proses = array_sum($this->input->post('prosesbaru'));
						// }else if ($this->input->post('proses') != NULL AND $this->input->post('prosesbaru') == NULL){
						// 	$proses = array_sum($this->input->post('proses'));
						// }

						$proses = array_sum(@$jml)+array_sum(@$jmlbaru);

						if ($q->num_rows() > 0) {
							$row = $q->row();
							$this->db->select('MIN(tgl_selesai) as tgl,id_jadwal,al_waktu,tgl_selesai');
							$this->db->from('penjadwalan');
							$this->db->join('work_order', 'penjadwalan.id_wo = work_order.id', 'INNER');
							$this->db->where('work_order.status_pekerjaan', '2');
							$this->db->where('penjadwalan.flag_booking', 0);
							$this->db->where('penjadwalan.stall !=', '');
							$cekrow = $this->db->get();
							if ($cekrow->num_rows() > 0) {
								$row1 = $cekrow->row();
								if (date('Y-m-d H:i:s',strtotime($this->input->post('waktumulai'))) < @$row1->tgl) {
									$this->db->trans_rollback();
									$response['code'] = 400;
									$response['message'] = 'Waktu Mulai Tidak Boleh Kurang Dari Tanggal '.@$row1->tgl.'';
									echo json_encode($response);
									return;
								}else{
									$idstallbook = $row1->id_jadwal;
									$tgterima = @$row1->tgl_terima;
									$tgselesai = @$row1->tgl_selesai;
									$aliran = round($row1->al_waktu + ($proses/8),1);
									$tanggal = date('Y-m-d H:i:s',strtotime(@$row1->tgl_selesai));
								}
							}else{
								$this->db->select('MIN(tgl_selesai) as tgl,id_jadwal,al_waktu,tgl_selesai');
								$this->db->from('penjadwalan');
								$this->db->join('work_order', 'penjadwalan.id_wo = work_order.id', 'INNER');
								$this->db->where('work_order.status_pekerjaan', '2');
								$this->db->where('penjadwalan.flag_booking', 0);
								$this->db->where('penjadwalan.stall', '');
								$cekrow1 = $this->db->get();
								$row2 = $cekrow->row();
								if (date('Y-m-d H:i:s',strtotime($this->input->post('waktumulai'))) < @$row2->tgl) {
									$this->db->trans_rollback();
									$response['code'] = 400;
									$response['message'] = 'Waktu Mulai Tidak Boleh Kurang Dari Tanggal '.@$row2->tgl.'';
									echo json_encode($response);
									return;
								}else{
									$idstallbook = @$row2->id_jadwal;
									$tgterima = @$row2->tgl_terima;
									$tgselesai = @$row2->tgl_selesai;
									$aliran = round(@$row2->al_waktu + ($proses/8),1);
									$tanggal = date('Y-m-d H:i:s',strtotime(@$row2->tgl_selesai));
								}
							}
							
							// return var_dump($tanggal);
						}else{
							$aliran = round($proses/8,1);
							$tanggal = date('Y-m-d H:i:s',strtotime($this->input->post('waktumulai')));
						}
						$prosesjam = round($proses/8,1);
						$coba = date_create($tanggal);
						date_add($coba,date_interval_create_from_date_string(floor($prosesjam).' days'));
						$converttanggal = date_format($coba,'Y-m-d H:i:s');

						// return var_dump($prosesjam);

						$expo = explode('.', $prosesjam);
						if (@$expo[1] != null) {
							$hasilexpo = round(($expo[1]/10)*8);
							$tttt = date_create($converttanggal);
							date_add($tttt,date_interval_create_from_date_string($hasilexpo.' hours'));
							$convertjam = date_format($tttt,'H');
							$converttanggalex = date_format($tttt,'Y-m-d H:i:s');
						}else{
							$convertjam = date_format($coba,'H');
							$converttanggalex = $converttanggal;
						}

						$startkerja = date('Y-m-d 09:i:s',strtotime($converttanggalex));
						$hasilsemua = date_create($startkerja);

						// return var_dump($convertjam);
						if ($convertjam == '00') {
							date_add($hasilsemua,date_interval_create_from_date_string('1 hours'));
							$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
						}else if ($convertjam == '09') {
							date_add($hasilsemua,date_interval_create_from_date_string('1 hours'));
							$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
						}else if ($convertjam >= '17') {
							$lebihjam5 = ($convertjam - 17);
							date_add($hasilsemua,date_interval_create_from_date_string($lebihjam5.' hours'));
							$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
						} else if ($convertjam <= '09') {
							date_add($hasilsemua,date_interval_create_from_date_string($convertjam.' hours'));
							$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
						}else{
							$totalconvert = $converttanggalex;
						}

						// return var_dump($totalconvert);
						if (@$hasilstall[0] == null) {
							$this->db->select('work_order.no_wo');
							$this->db->from('work_order');
							$this->db->where('md5(id)',$this->input->post('href'));
							$no_woalert =  $this->db->get();
							$this->db->update('penjadwalan', array('flag_booking'=>@$idstallbook),array('id_jadwal'=>@$idstallbook));
							$alert = "Maaf Tidak Ada Stall Tersedia. Kendaraan Dengan ".@$no_woalert->row()->no_wo." Akan Menunggu Sampai Stall Tersedia";
							// $tgltunggu = date('d',strtotime(@$tgterima));
							// $tglterima = date('d',strtotime(@$tgselesai));
							// if ($tglterima < $tgltunggu) {
							// 	$w_tunggu = $tgltunggu - $tglterima;
							// }else{
							// 	$w_tunggu = $tglterima - $tgltunggu;
							// }
							$date1=date_create(@$tgterima);
							$date2=date_create(@$tgselesai);
							$diff=date_diff($date1,$date2);
							$w_tunggu = $diff->format("%a");
						}else{
							$alert = "Berhasil Membuat Jadwal Service Kendaraan";
							$w_tunggu = 0;
						}
						$datajadwal = array(
							'w_proses'=>$proses,
							'stall'=>@$hasilstall[0],
							'al_waktu'=>$aliran,
							'w_mulai'=>$tanggal,
							'w_tunggu'=>intval(@$w_tunggu),
							'tgl_selesai'=>$totalconvert,
							'updated_by'=>$this->session->userdata('id'),
						);
						$wherejadwal = array(
							'md5(id_wo)'=>$this->input->post('href')
						);
						$jadwal = $this->db->update('penjadwalan', $datajadwal,$wherejadwal);
						if ($jadwal) {
							$this->db->update('work_order', array('status_pekerjaan'=>2),array('md5(id)'=>$this->input->post('href')));

							$this->db->trans_commit();
							$response['code'] = 200;
							$response['message'] = $alert;
						}else{
							$this->db->trans_rollback();

							$response['code'] = 400;
							$response['message'] = 'Gagal Edit Data Kerja';
						}
					}else{
						$this->db->trans_rollback();
						$response['code'] = 400;
						$response['message'] = 'Gagal Edit Data Penjadwalan';
					}
				}else{
					$this->db->trans_rollback();
					$response['code'] = 400;
					$response['message'] = validation_errors();
				}
			}
		}
		echo json_encode($response);
	}
	function DataIntruksi()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['link_view'] = 'admin/instruksi/datainstruksi';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function InputInstruksi()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Selamat Datang Di Halaman Dasboard';
			$data['link_view'] = 'admin/instruksi/inputinstruksi';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function getJenis()
	{

		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			$this->db->select('id_kategori,jenis_kategori');
			$this->db->from('kategori');
			$this->db->like('jenis_kategori', ucwords($this->input->get('q')), 'BOTH');
			$this->db->where('status', 1);
			$this->db->limit(4);
			$q = $this->db->get();
			foreach ($q->result() as $key) {
				$json[] = ['id'=>$key->id_kategori,'text'=>$key->jenis_kategori];
			}
		}
		echo json_encode($json);
	}
	function getStall()
	{
		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			$this->db->select('stall');
			$this->db->from('penjadwalan');
			$this->db->like('stall', ucwords($this->input->get('q')), 'BOTH');
			$this->db->group_by('stall');
			$this->db->limit(4);
			$q = $this->db->get();
			$json = array('success'=>true,'data'=>$q->result());
		}
		echo json_encode($json);
	}
	function getPart()
	{
		$this->db->select('id_part,nm_part');
		$this->db->from('data_part');
		$this->db->where('jml_part >', 0);
		$this->db->like('nm_part', ucwords($this->input->get('q')), 'BOTH');
		$this->db->group_by('nm_part');
		$q = $this->db->get();
		return $q;
	}
	function SelesaiInstruksi()
	{
		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'success'=>false,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id')==null) {
				redirect('/');
				return;
			}else{
				$this->form_validation->set_rules('id','idnya','required',array('required' => 'Mohon id jangan di hapus'));
				if ($this->form_validation->run() == true) {
					$this->db->trans_start();
					$cekdate = $this->db->get_where('penjadwalan', array('md5(id_wo)'=>$this->input->post('id')))->row();
					$this->db->select('sum(al_waktu) as aliran');
					$this->db->from('penjadwalan');
					$this->db->where('id_jadwal !=', @$cekdate->id_jadwal);
					$this->db->where('stall', @$cekdate->stall);
					$this->db->where('w_mulai <', @$cekdate->w_mulai);
					$alirannya = $this->db->get();

					$date1=date_create(@$cekdate->w_mulai);
					$date2=date_create(date('Y-m-d H:i:s'));
					$diff=date_diff($date1,$date2);
					// Jam
					$jam = $diff->format("%h");
					$minute = $diff->format("%i");

					$pen  = $this->db->update('penjadwalan', array('updated_at'=>date('Y-m-d H:i:s'),'al_waktu'=>(@$alirannya->row()->al_waktu + ($jam/8)),'w_proses'=>round($jam,1),'tgl_selesai'=>date('Y-m-d H:i:s')),array('md5(id_wo)'=>$this->input->post('id')));
					if ($pen) {
						$qork = $this->db->update('work_order', array('status_pekerjaan'=>3),array('md5(id)'=>$this->input->post('id')));
						if ($qork) {
							$this->db->select('penjadwalan.*');
							$this->db->from('penjadwalan');
							$this->db->join('work_order', 'work_order.id = penjadwalan.id_wo', 'INNER');
							$this->db->where('md5(penjadwalan.id_wo)', $this->input->post('id'));
							$q = $this->db->get();
							if ($q->num_rows() > 0) {
								$this->db->select('*');
								$this->db->from('penjadwalan');
								$this->db->where('stall', null);
								$this->db->where('w_mulai >=', date('Y-m-d H:i:s'));
								$this->db->order_by('created_at', 'ASC');
								$cek = $this->db->get();
								if ($cek->num_rows() > 0) {
									$al_waktu = @$cek->row()->w_proses/8 + @$q->row()->al_waktu;
									$tanggal = @$q->row()->updated_at;

									$prosesjam = round(@$cek->row()->w_proses/8,1);
									$coba = date_create($tanggal);
									date_add($coba,date_interval_create_from_date_string(floor($prosesjam).' days'));
									$converttanggal = date_format($coba,'Y-m-d H:i:s');

								// return var_dump($prosesjam);

									$expo = explode('.', $prosesjam);
									if (@$expo[1] != null) {
										$hasilexpo = round(($expo[1]/10)*8);
										$tttt = date_create($converttanggal);
										date_add($tttt,date_interval_create_from_date_string($hasilexpo.' hours'));
										$convertjam = date_format($tttt,'H');
										$converttanggalex = date_format($tttt,'Y-m-d H:i:s');
									}else{
										$convertjam = date_format($coba,'H');
										$converttanggalex = $converttanggal;
									}

									$startkerja = date('Y-m-d 09:i:s',strtotime($converttanggalex));
									$hasilsemua = date_create($startkerja);

								// return var_dump($convertjam);
									if ($convertjam == '00') {
										date_add($hasilsemua,date_interval_create_from_date_string('1 hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									}else if ($convertjam == '09') {
										//$hasilkurang = ($convertjam-1);
										date_add($hasilsemua,date_interval_create_from_date_string('1 hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									}else if ($convertjam >= '17') {
										$lebihjam5 = ($convertjam - 17);
										date_add($hasilsemua,date_interval_create_from_date_string($lebihjam5.' hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									} else if ($convertjam <= '09') {
										date_add($hasilsemua,date_interval_create_from_date_string($convertjam.' hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									}else{
										$totalconvert = $converttanggalex;
									}

									$this->db->update('penjadwalan', array('stall'=>@$q->row()->stall,'w_mulai'=>@$q->row()->updated_at,'al_waktu'=>@$al_waktu,'tgl_selesai'=>@$totalconvert),array('id_jadwal'=>$cek->row()->id_jadwal,'stall'=>null));
								}else{
									$this->db->trans_commit();
									$json['success'] = true;
									$json['message'] = 'Berhasil';
								}
								$this->db->trans_commit();
								$json['success'] = true;
								$json['message'] = 'Berhasil';
							}else{
								$this->db->trans_commit();
								$json['success'] = true;
								$json['message'] = 'Berhasil';
							}
						}else{
							$this->db->trans_rollback();
							$json['success'] = false;
							$json['message'] = 'Berhasil';
						}
					}else{
						$this->db->trans_rollback();
						$json['success'] = false;
						$json['message'] = 'Berhasil';
					}

				}
			}
		}
		echo json_encode($json);
	}
	function HapusInstruksi()
	{
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'success'=>false,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			$this->db->trans_start();
			$this->db->select('*');
			$this->db->from('jenis_pekerjaan');
			$this->db->where('md5(jenis_pekerjaan.id_jp)', $this->input->post('id_jp'));
			$querydata = $this->db->get();
			if ($querydata->num_rows() > 0) {
				$row = $querydata->row();
				$where = array(
					'id_jp'=>$row->id_jp
				);
				$q = $this->db->delete('jenis_pekerjaan',$where);
				if ($q) {
					$this->db->trans_commit();
					$json['success'] = true;
					$json['message'] = 'Berhasil Hapus Data WO';
				}else{
					$this->db->trans_rollback();
					$json['success'] = false;
					$json['message'] = 'Gagal Hapus Data WO';
				}
			}else{
				$this->db->trans_rollback();
				$json['success'] = false;
				$json['message'] = 'Gagal Hapus Data WO';
			}
		}
		echo json_encode($json);
	}
	function deleteWO()
	{
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'code'=>400,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			$this->db->trans_start();
			$this->db->select('work_order.*,penjadwalan.id_jadwal');
			$this->db->from('work_order');
			$this->db->join('penjadwalan', 'penjadwalan.id_wo = work_order.id', 'INNER');
			$this->db->where('md5(work_order.id)', $this->input->post('id'));
			$querydata = $this->db->get();
			if ($querydata->num_rows() > 0) {

				$row = $querydata->row();
				$where = array(
					'id'=>$row->id
				);
				$wherewo = array(
					'id_wo'=>$row->id
				);
				$this->db->delete('work_order',$where);
				$this->db->delete('data_kendaraan',$wherewo);
				$this->db->delete('jenis_pekerjaan',$wherewo);
				$this->db->delete('pelanggan',$wherewo);
				$q = $this->db->delete('penjadwalan',$wherewo);
				if ($q) {
					$this->db->update('penjadwalan', array('flag_booking'=>'0'),array('flag_booking'=>$row->id_jadwal));
					$this->db->trans_commit();
					$json['code'] = 200;
					$json['message'] = 'Berhasil Hapus Data WO';
				}else{
					$this->db->trans_rollback();
					$json['code'] = 400;
					$json['message'] = 'Gagal Hapus Data WO';
				}
			}else{
				$this->db->trans_rollback();
				$json['code'] = 400;
				$json['message'] = 'Gagal Hapus Data WO';
			}
		}
		echo json_encode($json);
	}
	function setting()
	{
		if ($this->session->userdata('id') != null) {
			$data['title'] = 'Halaman Pengaturan Akun';
			$data['link_view'] = 'admin/user/setting';
			$this->load->view('private',$data);
		}else{
			redirect('/');
		}
	}
	function EditAkun()
	{
		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'success'=>false,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id') != null) {
				$this->form_validation->set_rules('namaakun','Nama Akun','required',array('required' => 'Mohon Isi Nama Akun'));
				$this->form_validation->set_rules('username','Username','required',array('required' => 'Mohon Isi Username'));
				$this->form_validation->set_rules('nohp','Nomor Handphone','required',array('required' => 'Mohon Isi Nomor Handphone'));

				if ($this->form_validation->run() == true) {
					if ($this->input->post('pass') != null) {
						if ($this->input->post('pass') == $this->input->post('re_pass')) {
							$data = array(
								'name'=>$this->input->post('namaakun'),
								'username'=>$this->input->post('username'),
								'password'=>password_hash($this->input->post('pass'), PASSWORD_DEFAULT),
								'no_hp'=>$this->input->post('nohp'),
							);
						}else{
							$json['success']=false;
							$json['message']='Harus Sama';
						}
					}else{
						$data = array(
							'name'=>$this->input->post('namaakun'),
							'username'=>$this->input->post('username'),
							'no_hp'=>$this->input->post('nohp'),
						);
					}
					$q = $this->db->update('users', $data,array('id'=>$this->session->userdata('id')));
					if ($q) {
						$json['success']=true;
						$json['message']='Berhasil Ubah Akun';
					}else{
						$json['success']=false;
						$json['message']='Gagal Ubah Profile';
					}
				}else{
					$json['success']=false;
					$json['message']=validation_errors();
				}
			}else{
				$response['success'] = false;
				$response['message'] = 'Silakan Login Kembali';
			}
		}
		$this->session->sess_destroy();
		echo json_encode($json);
	}
	function logout()
	{
		$this->session->sess_destroy();
		redirect(site_url('/'));
	}
	function CronJadwal()
	{
		$json = array();
		if (!$this->input->is_ajax_request()) {
			$json = array(
				'success'=>false,
				'message'=> 'Tidak Boleh Akses'
			);
		}else{
			if ($this->session->userdata('id')==null) {
				redirect('/');
				return;
			}else{
				$this->form_validation->set_rules('id','idnya','required',array('required' => 'Mohon id jangan di hapus'));
				if ($this->form_validation->run() == true) {
					$this->db->trans_start();
					$this->db->set('updated_at',date('Y-m-d H:i:s'));
					$this->db->where('tgl_selesai', date('Y-m-d H:i:s'));
					$pen  = $this->db->update('penjadwalan');
					if ($pen) {
						$this->db->select('*');
						$this->db->from('penjadwalan');
						$this->db->join('work_order', 'work_order.id = penjadwalan.id_wo', 'INNER');
						$this->db->where('tgl_selesai <=', date('Y-m-d H:i:s'));
						$this->db->where('status_pekerjaan', 2);
						$q = $this->db->get();
						if ($q->num_rows() > 0) {
							$qork = $this->db->update('work_order', array('status_pekerjaan'=>3),array('id'=>$q->row()->id_wo));
							if ($qork) {
								$this->db->select('penjadwalan.*');
								$this->db->from('penjadwalan');
								$this->db->join('work_order', 'work_order.id = penjadwalan.id_wo', 'INNER');
								$this->db->where('penjadwalan.id_wo', $q->row()->id_wo);
								$q2 = $this->db->get();
								if ($q2->num_rows() > 0) {
									$this->db->select('*');
									$this->db->from('penjadwalan');
									$this->db->where('stall', null);
									$this->db->where('w_mulai >=', date('Y-m-d H:i:s'));
									$this->db->order_by('created_at', 'ASC');
									$cek = $this->db->get();
									$al_waktu = @$cek->row()->w_proses/8 + @$q2->row()->al_waktu;
									$tanggal = @$q->row()->tgl_selesai;

									$prosesjam = round(@$cek->row()->w_proses/8,1);
									$coba = date_create($tanggal);
									date_add($coba,date_interval_create_from_date_string(floor($prosesjam).' days'));
									$converttanggal = date_format($coba,'Y-m-d H:i:s');

								// return var_dump($prosesjam);

									$expo = explode('.', $prosesjam);
									if (@$expo[1] != null) {
										$hasilexpo = round(($expo[1]/10)*8);
										$tttt = date_create($converttanggal);
										date_add($tttt,date_interval_create_from_date_string($hasilexpo.' hours'));
										$convertjam = date_format($tttt,'H');
										$converttanggalex = date_format($tttt,'Y-m-d H:i:s');
									}else{
										$convertjam = date_format($coba,'H');
										$converttanggalex = $converttanggal;
									}

									$startkerja = date('Y-m-d 09:i:s',strtotime($converttanggalex));
									$hasilsemua = date_create($startkerja);

								// return var_dump($convertjam);
									if ($convertjam == '00') {
										date_add($hasilsemua,date_interval_create_from_date_string('1 hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									}else if ($convertjam == '09') {
										//$hasilkurang = ($convertjam-1);
										date_add($hasilsemua,date_interval_create_from_date_string('1 hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									}else if ($convertjam >= '17') {
										$lebihjam5 = ($convertjam - 17);
										date_add($hasilsemua,date_interval_create_from_date_string($lebihjam5.' hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									} else if ($convertjam <= '09') {
										date_add($hasilsemua,date_interval_create_from_date_string($convertjam.' hours'));
										$totalconvert = date_format($hasilsemua,'Y-m-d H:i:s');
									}else{
										$totalconvert = $converttanggalex;
									}
									$this->db->update('penjadwalan', array('stall'=>@$q2->row()->stall,'w_mulai'=>@$q2->row()->updated_at,'al_waktu'=>round($al_waktu),'tgl_selesai'=>@$totalconvert),array('id_jadwal'=>$cek->row()->id_jadwal,'stall'=>null));
								}else{
									$this->db->trans_commit();
									$json['success'] = true;
									$json['message'] = 'Berhasil';
								}
								$this->db->trans_commit();
								$json['success'] = true;
								$json['message'] = 'Berhasil';
							}else{
								$this->db->trans_rollback();
								$json['success'] = false;
								$json['message'] = 'Berhasil';
							}

						}else{
							$this->db->trans_rollback();
							$json['success'] = false;
							$json['message'] = 'Gagal Data Kosong';
						}
					}else{
						$this->db->trans_rollback();
						$json['success'] = false;
						$json['message'] = 'Gagal';
					}
				}else{
					$this->db->trans_rollback();
					$json['success'] = false;
					$json['message'] = 'Gagal';
				}
			}
		}
		echo json_encode($json);
	}
	function getTanggalMulai()
	{
		$this->db->select('MIN(tgl_selesai) as tgl');
		$this->db->from('penjadwalan');
		$this->db->join('work_order', 'penjadwalan.id_wo = work_order.id', 'INNER');
		$this->db->where('work_order.status_pekerjaan', 2);
		$this->db->where('penjadwalan.flag_booking', 0);
		$this->db->where('penjadwalan.stall !=', '');
		$this->db->order_by('tgl_selesai', 'ASC');
		$cekrow = $this->db->get();
		if ($cekrow->num_rows() > 0) {
			$row1 = $cekrow->row();
			$tanggal = date('d-m-Y H:i:s',strtotime(@$row1->tgl));
		}else{
			$this->db->select('MIN(tgl_selesai) as tgl');
			$this->db->from('penjadwalan');
			$this->db->join('work_order', 'penjadwalan.id_wo = work_order.id', 'INNER');
			$this->db->where('work_order.status_pekerjaan', 2);
			$this->db->where('penjadwalan.flag_booking', 0);
			$this->db->where('penjadwalan.stall', '');
			$this->db->order_by('tgl_selesai', 'ASC');
			$cekrow1 = $this->db->get();
			$row2 = $cekrow->row(); 
			$tanggal = date('d-m-Y H:i:s',strtotime(@$row2->tgl));
		}
		echo json_encode(array('data'=>@$tanggal));
	}
}

/* End of file AdminContoller.php */
/* Location: ./application/controllers/AdminContoller.php */