$("#formlogin").submit(function (event) {
		var data = new FormData($(this)[0]);
		$.ajax({
			url: BASE_URL+'IndexController/actionlogin',
			type: "POST",
			dataType:'json',
			data: data,
			contentType: false,
			cache: false,
			processData: false,
			beforeSend:function(argument) {
				$("#text-loader").html('Mohon Tunggu');
				$('#page-loader').fadeIn('fast');
			},
			success: function (response) {
				if (response.success == false) {
					Swal.fire(
						''+response.msg+'',
						);
				}else{
					window.location.href= BASE_URL;
				}
				$('#page-loader').fadeOut('fast'); 
			},
			error: function () {
				Swal.fire(
					'"'+response.msg+'"',
					'Hubungi Tim Terkait',
					);
				$('#page-loader').fadeOut('fast'); 
			}
		});
	});